There once was a crow.  The crow was sitting down on the branch of a tree.  A piece of some cheese was in the beak of the crow.  

A fox observed the crow and set the wits of the fox in order for he to get the cheese.  

The fox came and stood near the tree.  

The fox looked toward the crow.  

The fox said that a noble bird was above him.  

The fox said that the beauty of a bird was exceptional.  

The fox said that the hue of the plumage of a bird was exquisite.  

The fox said that -- if the voice of a bird is equal to the good looks of a bird -- a bird would be undoubtedly obligated to be the queen of every bird.  

The fox excessively flattered the crow because he had said that the beauty of a bird was exceptional and because the fox had said that the hue of the plumage of a bird was exquisite.  

The crow cawed loudly in order to show to the fox that she was able to sing.  

The fox snatched the cheese, and the cheese fell naturally.  

The fox said that he believed that the crow was able to sing and said that the crow needed wits.  

