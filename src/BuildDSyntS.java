import generator.aggregation.elements.Relation;
import java.math.*;
import java.util.*;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import story.scheherazade.admin.StoryInterpreter;
import story.scheherazade.misc.TimelineID;
import story.scheherazade.prologuer.PredicateMode;
import story.scheherazade.verbalizer.VerbalizerState;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import story.dbcore.exceptions.Scheherexception;

/**
 * constructs a dsynts from the features identified in each SchLexSynt node
 *
 * saveDSynts
 * saveDSynts2Domain
 */
public class BuildDSyntS {
	/** a list of propositions
	* each element is a root of the tree-like structure of lexico-syntactic units */
	public static ArrayList<SchLexSynt> dsynt_list;
    public static ArrayList<SchLexSynt> unsplit_dsynt_list;

	/** stores the (propositionID; satellite; relation) triples */
    public static TextPlan[] textplans;
	
	/** flag that controls whether by default
	* events will be aggregated using temporal ordering on the Sch Timeline
	* or verbalized as independent simple sentences
	* Temoral aggregation is naive,
	*	doesn't account for the grammatical aspect of verb,
	*	hence following bug:
	* Ex: *After the grapes hang on the vine, the fox tried to reach the grapes */
    private static boolean useTemporalAggregation = true;

    /**
     *
     */
	public BuildDSyntS(){
		dsynt_list = new ArrayList<SchLexSynt>();
        unsplit_dsynt_list = new ArrayList<SchLexSynt>();
	}

    public void initTextPlans() {
        textplans = new TextPlan[dsynt_list.size()*2];
    }

    /**
     * Adds a SchLexSynt (SchVerb) head node to the list of dsynts in this story
     * @param head
     */
	public void addDSynt(SchLexSynt head){
		dsynt_list.add(head);
	}

    /**
     * Returns the dsynts list for this story.
     *  Each item in the list is a sentence head verb
     * @return the dsynts for this story
     */
    public ArrayList<SchLexSynt> getDsynts() {return dsynt_list;}

    // create a deep copy of the original dsynts
    public void copyDsynts() {
        unsplit_dsynt_list = new ArrayList<SchLexSynt>();
        for (SchLexSynt root_verb: dsynt_list) {
            SchVerb newVerb = ((SchVerb)root_verb).createCopy();
            unsplit_dsynt_list.add(newVerb);
        }
    }

    // copies the original dsynts from unsplit list back to the current dsynt_list
    public void resetDsynts() {
        dsynt_list = new ArrayList<SchLexSynt>();
        for (SchLexSynt root_verb: unsplit_dsynt_list) {
            SchVerb newVerb = ((SchVerb)root_verb).createCopy();
            dsynt_list.add(newVerb);
        }
    }

    /**
     * When transforming dsynts from 3rd to 1st person, we change some
     * attributes of the nouns. Need to reset them back to neutral 3rd person voice
     * @param node any SchLexSynt node
     * @throws Scheherexception
     */
    public void changePossession(SchLexSynt node, String characterName, boolean speech) throws Scheherexception {
        if (node instanceof SchNoun && ((SchNoun)node).lexeme.equals(characterName) && !speech) {
            ((SchNoun)node).setFirstPerson(true);
        }
        for (SchLexSynt child: node.getChildren()) {
            // handles 'the narrator's apartment' -> 'my apartment', not 'the my's apartment'
            if (node instanceof SchNoun && child instanceof SchNoun) {
                if (((SchNoun) child).lexeme != null && ((SchNoun)child).lexeme.equals(characterName)) {
                    ((SchNoun) node).article = "no-art";
                }

                // nested grandchildren
                for (SchLexSynt grandchild: child.getChildren()) {
                    if (grandchild instanceof SchNoun) {
                        if (((SchNoun) grandchild).lexeme != null && ((SchNoun)grandchild).lexeme.equals(characterName)) {
                            ((SchNoun) node).article = "no-art";
                        }

                    }
                }
            }
            changePossession(child, characterName, speech);
        }
    }

    /**
     * Transforms SchLexSynt objects into Java XML node formalism
     * @param doc
     * @param head
     * @return the head node for each sentence in an xmlNode format
     */
	private Element xmlNode(Document doc, SchLexSynt head){
		Element node = doc.createElement("dsyntnode");
		for (Map.Entry<String, String> features : head.getFeatures().entrySet()) {
			node.setAttribute(features.getKey(), features.getValue());
		}
		for (SchLexSynt child : head.getChildren()){
			node.appendChild(xmlNode(doc, child));
		}
		return node;
	}

    /**
     * Transforms all story dsynts into a single xml file
     * that can be processed by RealPro
     * @param file_name
     */
	public void saveDSynt(String file_name) {	 
		  try {
	 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
            // output one simple xml file for all of the dsynts in the story
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("dsynts-list");
			doc.appendChild(rootElement);
			
			for (int i = 0; i < dsynt_list.size(); i++) {
				Element element = doc.createElement("dsynts");
				element.setAttribute("id", Integer.toString(i));
                rootElement.appendChild(element);
				// recursively creates xmlNodes for head and its children
				element.appendChild(this.xmlNode(doc, dsynt_list.get(i)));
			}
	 
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource source = new DOMSource(doc);
			File file = new File(file_name); 
			// if file doesn't exists, create it
			if (!file.exists()) {
				file.createNewFile();
			}
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
	 
			System.out.println("XML file " + file_name + " saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  } catch (IOException e) {
				e.printStackTrace();
		}
	}

    /**
     * Creates a story folder inside the specified Personage domain
     * then creates xml file for each dsynt and saves them in the new directory
     * finally calls method to build textplans
     * @param domain
     * @param story_name
     */
    public void saveDSynt2Domain(String domain, String story_name) {

        try {
            String dsynt_path = domain + "/dsynts/" + story_name + "/";
            File file = new File(dsynt_path);
            if (!file.exists()) {file.mkdir();}
            else { // remove files in dir
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    fileDelete.delete();
                }
            }

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // creates a new xml file for each dsynt in the story
            for (int i = 0; i < dsynt_list.size(); i++) {
                // root elements
                Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("dsynts-list");
                doc.appendChild(rootElement);

                Element element = doc.createElement("dsynts");
                element.setAttribute("id", Integer.toString(i)); // this id will match the file name/number
                rootElement.appendChild(element);

                SchLexSynt unit = dsynt_list.get(i);
                element.appendChild(this.xmlNode(doc, unit));

                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                DOMSource source = new DOMSource(doc);
                file = new File(dsynt_path + Integer.toString(i) + ".xml");
                // if file doesn't exists, create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                StreamResult result = new StreamResult(file);
                transformer.transform(source, result);
            }

            // builds the text plans
            buildTextPlans2Domain(domain, story_name);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Creates Text Plans
     * Adds them to the textplans array;
     * Supports RST relations and Direct speech
     *
     * @throws Scheherexception
     */
    public void splitDSynts(StoryInterpreter storyInterpreter, Parameters params, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        SchLexSynt head;
        SchLexSynt newHead;
        // need a new data structue to keep track of splits and non-splits
        // when we finish, we reassign dsynts to new_dsynts
        ArrayList<SchLexSynt> new_dsynts = new ArrayList<SchLexSynt>();

        for (int i = 0; i < dsynt_list.size(); i++) {
            boolean directSpeech = false; // init
            boolean deaggregate = false; // init
            head = dsynt_list.get(i); // head verb for each sentence
            newHead = null;

            // direct speech check + some probability from param.getDirectSpeech
            if (params.getNarr_directSpeech() > 0 && Math.random()*100 < params.getNarr_directSpeech()) {

                // get the second half of the split
                // first check for direct speech
                newHead = splitDirectSpeech(head, newHead, storyInterpreter, timeline, verbalizerState);
                if (newHead != null) {
                    directSpeech = true;

                    //. formatting for personage
                    //new_dsynts.add(newHead);
                    //new_dsynts.add(head);
                    //textplans[new_dsynts.indexOf(newHead)] = new TextPlan(Integer.toString(new_dsynts.indexOf(newHead)), Integer.toString(new_dsynts.indexOf(head)), "speech");

                    new_dsynts.add(head);
                    new_dsynts.add(newHead);
                    textplans[new_dsynts.indexOf(newHead)] = new TextPlan(Integer.toString(new_dsynts.indexOf(newHead)), Integer.toString(new_dsynts.indexOf(head)), "speech");

                    // first extract the speaker by iterating over the children of the verb.
                    // we are looking for a SchNoun child with rel=I (i.e. index=0)
                    String speaker_type = "", speaker_name = "";
                    for (SchLexSynt child : head.getChildren()) {
                        if (child instanceof SchNoun && ((SchNoun) child).index == 0) {
                            speaker_type = ((SchNoun) child).getLexeme();
                            speaker_name = ((SchNoun) child).name;
                            break;  // don't need to keep looking after we found the speaker
                        }
                    }
                    if (!speaker_name.equals(""))
                        textplans[new_dsynts.indexOf(newHead)].setVoice(speaker_type + "-" + speaker_name);
                    else
                        textplans[new_dsynts.indexOf(newHead)].setVoice(speaker_type);
                }
            }

            // if not direct speech, check for deagg
            if (!directSpeech && params.getSent_deagg() > 0) {
                if (Math.random()*100 < params.getSent_deagg()) {
                    // if we find a split, need to remove the second half from the first half so there is no overlap + some probability from param.getDirectSpeech
                    newHead = splitDsynts(head, null);
                    if (newHead != null) {
                        deaggregate = true;
                        SchVerb first_verb = (SchVerb) newHead.getChildren().get(0); // first verb after split

                        // BECAUSE
                        if (newHead instanceof SchFunc && ((SchFunc) newHead).getLexeme().equals("because")) {
                            head.getChildren().remove(newHead);
                            newHead = first_verb;
                            new_dsynts.add(newHead);
                            new_dsynts.add(head);

                            // create a text plan with this relationship: contingency <- because
                            //textplans[new_dsynts.indexOf(newHead)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "contingency_because");
                            textplans[new_dsynts.indexOf(newHead)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "contingency_cause");
                        }

                        // IN ORDER
                        else if (newHead instanceof SchFunc && ((SchFunc) newHead).getLexeme().equals("in_order")) {
                            SchVerb want = new SchVerb("verb1825237_want_something_wants_some_proposition");

                            // iterate through children of first_verb, find the subject, remove it and add it to be a child of want
                            for (int j = 0; j < first_verb.getChildren().size(); j++) {
                                if (first_verb.getChildren().get(j) instanceof SchNoun) {
                                    want.setChild(first_verb.getChildren().get(j)); // add the subject of the first verb
                                    first_verb.getChildren().remove(j);
                                }
                            }

                            head.getChildren().remove(newHead);

                            want.setChild(first_verb);
                            newHead = want;

                            new_dsynts.add(head);
                            new_dsynts.add(newHead);

                            //textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "contingency_in-order-to");
                            textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "contingency_cause");
                        }

                        // CONTRAST
                        else if (newHead instanceof SchFunc && ((SchFunc) newHead).getLexeme().equals("but")) {
                            head.getChildren().remove(newHead);
                            newHead = first_verb;
                            new_dsynts.add(newHead);
                            new_dsynts.add(head);
                            textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "comparison_contrast");
                        }

                        // COORDINATION, just any old verb
                        // TODO: under testing
                        /**else if (newHead instanceof SchVerb) {
                         head.getChildren().remove(newHead);
                         newHead = first_verb;
                         new_dsynts.add(newHead);
                         new_dsynts.add(head);
                         textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), Integer.toString(new_dsynts.indexOf(newHead)), "synchronous");
                         }
                         **/
                    }
                }
            }

            // no split, append dsynt normally
            if (!directSpeech && !deaggregate) {
                new_dsynts.add(head);
                textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), "", "None");
            }
        }

        dsynt_list = new_dsynts;
    }

    // A default when we're not doing any aggregation or deaggregation
    public void defaultTextPlans() throws Scheherexception {
        SchLexSynt head, newHead;
        // need a new data structure to keep track of splits and non-splits
        // when we finish, we reassign dsynts to new_dsynts
        ArrayList<SchLexSynt> new_dsynts = new ArrayList<SchLexSynt>();

        for (int i = 0; i < dsynt_list.size(); i++) {
            head = dsynt_list.get(i); // head verb for each sentence

            new_dsynts.add(head);
            textplans[new_dsynts.indexOf(head)] = new TextPlan(Integer.toString(new_dsynts.indexOf(head)), "", "None");
        }
        dsynt_list = new_dsynts;
    }


    /**
     * De-aggregation on Scheherazade modifiers e.g. because, in order
     * Prereq: There exists some modifier in this sentence
     * Find where it is and split the nodes into two SchLexSynt lists
     * Not dealing with text plans yet; just splitting the tree here
     * newHead: init null
     * head: entire sentence
     * return newHead: second half of sentence
     **/
    public SchLexSynt splitDirectSpeech(SchLexSynt node, SchLexSynt newHead, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        // if head verb is verb of communication, then find verb child and split
        String[] blacklist = {"measure", "discover", "ask", "talk", "require", "request", "sign", "order", "agree", "catalogue", "decide", "check", "expect"}; //don't want speech for these

        if (node instanceof SchVerb && ((SchVerb)node).isVerbOfSpeech() && !(Arrays.asList(blacklist).contains(((SchVerb)node).getLexeme()))
                && (((SchVerb)node).getLexeme().equals("say") || ((SchVerb)node).getLexeme().equals("think"))) {
            // iterate through children to find verb
            boolean directSpeech = true;
            if (directSpeech) {
                SchVerb utterance = null;
                String speaker_type = "", speaker_name = "";

                // first, we find the main verb that will be the root
                for (int i = 0; i < node.getChildren().size(); i++) {
                    SchLexSynt child = node.getChildren().get(i);
                    if (child instanceof SchVerb) {
                        utterance = ((SchVerb)child).createCopy();
                        utterance.setTense(((SchVerb) child).getTense());
                        utterance.setToInf(InfType.NoInf);
                        utterance.setMood("ind");
                        utterance.mode = PredicateMode.ActionAssert;
                        utterance.isSpeech = true;
                        node.getChildren().remove(child);
                        break;
                    }
                }

                // next we append any children of the main verb to our new root
                // unless it's an adverb
                if (utterance != null) {
                    for (int i = 0; i < node.getChildren().size(); i++) {
                        SchLexSynt child = node.getChildren().get(i);
                        if (child instanceof SchNoun && ((SchNoun)child).index == 0) {
                            speaker_type = ((SchNoun)child).lexeme;
                            speaker_name = ((SchNoun)child).name;
                        }
                        else {
                            if (child instanceof SchAdverb) {continue;}
                            utterance.setChild(child);
                        }
                    }

                    // remove child from node
                    for (SchLexSynt child: utterance.getChildren()) {
                        if (node.getChildren().contains(child)) {
                            node.getChildren().remove(child);
                        }
                    }

                    utterance.speakerName = speaker_name;
                    utterance.speakerType = speaker_type;

                    // pass along speaker name to children
                    // then if subj of verb == subj of governed verb ==> replace with 'I' and change person from 3d to first
                    setSpeaker(utterance, speaker_type, speaker_name);

                    // if we have a "the narrator" change it to "you"
                    setInterlocutor(utterance, speaker_type, speaker_name);

                    // make sure the tense is set
                    utterance.setTense(utterance.getTense());
                }

                newHead = utterance;
            }

        }

        return newHead;
    }

    /**
     * De-aggregation on Scheherazade modifiers e.g. because, in order
     * Prereq: There exists some modifier in this sentence
     * Find where it is and split the nodes into two SchLexSynt lists
     * Not dealing with text plans yet; just splitting the tree here
     * newHead: init null
     * head: entire sentence
     * return newHead: second half of sentence
     **/
    public SchLexSynt splitDsynts(SchLexSynt node, SchLexSynt newHead) throws Scheherexception {
        //  check for deagg
        if (node instanceof SchFunc) {
            if (((SchFunc) node).getLexeme().equals("because") || ((SchFunc) node).getLexeme().equals("in_order")) {
                // can we make the assumption that there is only one child of a because/in order and that it is a SchVerb?
                newHead = node;
                return newHead;
            }
        }

        // synchronous verbs
        // TODO: Under testing
        /**
        else if (node instanceof SchVerb) {
            SchLexSynt prev = node.getChildren().get(0);
            for (int i = 1; i < node.getChildren().size(); i++){
                SchLexSynt child = node.getChildren().get(i);
                if (prev.getClass() == child.getClass()){
                    if (prev instanceof SchVerb){
                        newHead = child;
                        return newHead;
                    }
                }
                prev = child;
            }
        }
         **/

        for (int i = 0; i < node.getChildren().size(); i++) {
            SchLexSynt child = node.getChildren().get(i);
            newHead = splitDsynts(child, newHead);

            if (newHead != null) {
                return newHead;
            }
        }

        return newHead;
    }

    /**
     * Recursively change nouns from 3rd person to 1st if the character is a narrator or if we are in a direct speech frame
     * speaker_type: noun, speaker_name: optional character name
     **/
    public static void setSpeaker(SchLexSynt node, String speaker_type, String speaker_name) {
        for (SchLexSynt child: node.getChildren()) {
            // check for noun children
            if (child instanceof SchNoun) {((SchNoun)child).setSpeaker(speaker_type, speaker_name, ((SchNoun) child).possessive); }

            // handle nested functional words and children of nouns (possessive)
            setSpeaker(child, speaker_type, speaker_name);
        }
    }

    public static void setInterlocutor(SchLexSynt node, String speaker_type, String speaker_name) {
        for (SchLexSynt child: node.getChildren()) {
            // check for noun children
            if (child instanceof SchNoun) {((SchNoun)child).setInterlocutor(speaker_type, speaker_name, ((SchNoun) child).possessive); }

            // handle nested functional words and children of nouns (possessive)
            setInterlocutor(child, speaker_type, speaker_name);
        }
    }

    /**
     * Creates textplans of story sentences for Personage
     * saves them into a story directory inside /textplans/ folder in corresponding Personage domain
     * default textplan : no rst, just proposition id
     *
     * Example xml file:
     * <?xml version="1.0" encoding="us-ascii"?>
     * <speechplan>
     *  <rstplan>
     *      <relation name="justify">
     *          <proposition ns="nucleus" id="1"/>
     *          <proposition ns="satellite" id="2"/>
     *      </relation>
     *  </rstplan>
     *  <proposition id="1" dialogue_act="7"/>
     *  <proposition id="2" dialogue_act="8"/>
     * </speechplan>
     *
     * Example xml file:
     * <?xml version="1.0" encoding="us-ascii"?>
     * <speechplan>
     *  <proposition id="1" dialogue_act="7"/>
     * </speechplan>
     * @param domain
     * @param story_name
     */
	public static void buildTextPlans2Domain(String domain, String story_name) {
        try {
            String textplan_path = domain + "/textplans/" + story_name + "/";
            File file = new File(textplan_path);
            if (!file.exists()) {file.mkdir();}
            else { // remove files in dir
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    fileDelete.delete();
                }
            }

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Integer id = 0;
            for (TextPlan plan : textplans) {
                id += 1;
                //List<String> tuple = textplans.get(proposition);
                if (plan == null || plan.isDummy()) {
                    continue;
                }

                String proposition = plan.getNucleus();
                String satelite = plan.getSatelite();
                String relation = plan.getRelation();

                Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("speechplan");
                String voice = plan.getVoice();
                if (voice.length() > 0)
                    rootElement.setAttribute("voice", voice);
                doc.appendChild(rootElement);

                if (!satelite.isEmpty()) {
                    Element element = doc.createElement("relation");
                    element.setAttribute("name", relation);
                    Element prop = doc.createElement("proposition");
                    prop.setAttribute("ns", "nucleus");
                    prop.setAttribute("id", "1");
                    element.appendChild(prop);
                    prop = doc.createElement("proposition");
                    prop.setAttribute("ns", "satellite");
                    prop.setAttribute("id", "2");
                    element.appendChild(prop);
                    Element rst = doc.createElement("rstplan");
                    rst.appendChild(element);
                    rootElement.appendChild(rst);
                }

                Element prop = doc.createElement("proposition");
                prop.setAttribute("id", "1");
                prop.setAttribute("dialogue_act", proposition);
                rootElement.appendChild(prop);
                if (!satelite.isEmpty()) {
                    prop = doc.createElement("proposition");
                    prop.setAttribute("id", "2");
                    prop.setAttribute("dialogue_act", satelite);
                    rootElement.appendChild(prop);
                }

                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                DOMSource source = new DOMSource(doc);
                file = new File(textplan_path + Integer.toString(id) + ".xml");
                // if file doesn't exists, create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                StreamResult result = new StreamResult(file);
                transformer.transform(source, result);

                // Output to console for testing
                StringWriter writer = new StringWriter();
                transformer.transform(new DOMSource(doc), new StreamResult(writer));
                String output = writer.getBuffer().toString();
                System.out.println(output);
            }
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void buildTextPlans(String file_name) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("textplan-list");
            doc.appendChild(rootElement);

            for (int i = 0; i < textplans.length; i++) {
                TextPlan plan = textplans[i];
                if (plan == null || plan.isDummy()) {
                    continue;
                }

                String proposition = plan.getNucleus();
                String satelite = plan.getSatelite();
                String relation = plan.getRelation();

                Element textplanElement = doc.createElement("textplan");
                textplanElement.setAttribute("id", Integer.toString(i));
                // voice element
                String voice = plan.getVoice();
                if (voice.length() > 0)
                    textplanElement.setAttribute("voice", voice);
                rootElement.appendChild(textplanElement);

                if (!satelite.isEmpty()) {
                    Element element = doc.createElement("relation");
                    element.setAttribute("name", relation);
                    Element prop = doc.createElement("proposition");
                    prop.setAttribute("ns", "nucleus");
                    prop.setAttribute("id", "1");
                    element.appendChild(prop);
                    prop = doc.createElement("proposition");
                    prop.setAttribute("ns", "satellite");
                    prop.setAttribute("id", "2");
                    element.appendChild(prop);
                    Element rst = doc.createElement("rstplan");
                    rst.appendChild(element);
                    textplanElement.appendChild(rst);
                }

                Element prop = doc.createElement("proposition");
                prop.setAttribute("id", "1");
                prop.setAttribute("dialogue_act", proposition);
                textplanElement.appendChild(prop);
                if (!satelite.isEmpty()) {
                    prop = doc.createElement("proposition");
                    prop.setAttribute("id", "2");
                    prop.setAttribute("dialogue_act", satelite);
                    textplanElement.appendChild(prop);
                }
            }

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            File file = new File(file_name);
            // if file doesn't exists, create it
            if (!file.exists()) {
                file.createNewFile();
            }
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);

            System.out.println("Text plan file saved!");

            // Output to console for testing
            //StringWriter writer = new StringWriter();
            //transformer.transform(new DOMSource(doc), new StreamResult(writer));
            //String output = writer.getBuffer().toString();
            //System.out.println(output);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}	


