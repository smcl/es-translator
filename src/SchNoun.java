import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.Map;

import story.dbcore.exceptions.Scheherexception;
import story.dbcore.misc.selfcontained.WordNetOffset;
import story.scheherazade.admin.StoryInterpreter;
import story.scheherazade.elements.ValidAction;
import story.scheherazade.elements.ValidNoun;
import story.scheherazade.misc.*;
import story.scheherazade.parameters.SCHArguments;
import story.scheherazade.prologuer.NounFlavor;
import story.scheherazade.verbalizer.VerbalizerState;


/**
  * Lexico-syntactic class for Nouns
 */
public class SchNoun implements SchLexSynt{
    ValidNoun validNoun;
    /** character's name (nickname) */
    NounName nounName;

    String name;

    String nounNameID = "";

    /** */
	boolean plural;

    /** */
	boolean proper;

    String pluralForm;

    /** can be one of: characterName, propName, qualityName */
	String IDType;

    /** wordnet offset  */
    String offset;

    /** */
	String lexeme, origLexeme;

    /** */
	NounFlavor flavor;

    /** */
	CharacterGender gender;

    /** */
	PropDiscreteness discreteness;

    /** */
	int index;

    /** */
	String article;

    /** */
	ArrayList<SchLexSynt> children;
    SchLexSynt parent;

    /** */
    boolean firstPerson, origFirstPerson;

    /** */
    boolean coreference;

    /** */
    String pronoun, origPronoun;

    /** */
    boolean possessive;

    ArrayList<String> properties; // if there is an adjective describing this character and we are in teh first person, put it here


    public SchNoun(ValidNoun noun, String charName, int noun_ind, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) {
        validNoun = noun;
        children = new ArrayList<SchLexSynt>();
        nounName = null;
        parent = null;
        name = "";
        nounNameID = "";
        plural = false;
        proper = false;
        pluralForm = "";
        IDType = null;
        offset = "";
        lexeme = "";
        flavor = null;
        gender = null;
        discreteness = null;
        index = -1;
        article = "";
        firstPerson = false;
        coreference = false;
        pronoun = "";
        possessive = false;
        properties = new ArrayList<String>();
        if (storyInterpreter != null) // don't do this if we don't have this info
            createNoun(noun, charName, noun_ind, storyInterpreter, timeline, verbalizerState);
    }

    public void initNoun(ValidNoun noun, String charName, int noun_ind) {
        try {
            nounNameID = charName;
            gender = noun.getGender();
            parent = null;
            NounType type = noun.getType(); // characterType, locationType
            IDType = type.getIDType().toString();
            discreteness = type.getDiscreteness();
            flavor = type.getNounFlavor();
            proper = false;
            pluralForm = type.getPluralForm();
            firstPerson = false;
            origFirstPerson = false;
            pronoun = "";
            origPronoun = "";
            coreference = false;
            article = null;
            index = noun_ind;
            lexeme = type.getMenuName(); // e.g. "narrator"
            origLexeme = type.getMenuName();

            WordNetOffset WNoffset = type.getWordNetOffset();
            if (WNoffset != null) {
                offset = WNoffset.getOffset().replace("noun_","");}
            else {offset = "-1";}

            if (!charName.contains("anon")) {name = charName;}
        } catch (Scheherexception e) {e.printStackTrace();}
    }

    /**
     * Deep SchNoun copy
     * @return
     */
    public SchNoun createCopy() {
        SchNoun newNoun = new SchNoun(this.validNoun, this.nounNameID, this.index, null, null, null);
        newNoun.nounName = this.nounName;
        newNoun.parent = this.parent;
        newNoun.name = this.name;
        newNoun.nounNameID = this.nounNameID;
        newNoun.plural = this.plural;
        newNoun.proper = this.proper;
        newNoun.pluralForm = this.pluralForm;
        newNoun.IDType = this.IDType;
        newNoun.offset = this.offset;
        newNoun.lexeme = this.lexeme;
        newNoun.flavor = this.flavor;
        for (SchLexSynt child : this.children) {
            newNoun.children.add(child.createCopy());
        }
        newNoun.gender = this.gender;
        newNoun.discreteness = this.discreteness;
        newNoun.index = this.index;
        newNoun.article = this.article;
        newNoun.firstPerson = this.firstPerson;
        newNoun.coreference = this.coreference;
        newNoun.pronoun = this.pronoun;
        newNoun.possessive = this.possessive;
        newNoun.properties = this.properties;
        return newNoun;
    }

    public void createNoun(ValidNoun noun, String charName, int noun_ind, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) {
        try {
            String nounTypeInfo = noun.getType().toString(); // e.g. "<noun10345804:narrator>"

            // 0) base case
            if (lexeme != null && !nounTypeInfo.contains("GROUP") && !nounTypeInfo.contains("something") && !nounTypeInfo.contains("EVERY") && !nounTypeInfo.contains("GENERIC")
                    && !nounTypeInfo.contains("abstract") && !nounTypeInfo.contains("Root Level") && !nounTypeInfo.contains("doing some action")) {
                if (charName.contains("abstract"))
                    initNoun(noun, "", noun_ind);
                else {
                    initNoun(noun, charName, noun_ind);
                }
            }

            // 1) "group"
            else if (nounTypeInfo.contains("GROUP")) {
                Object argument;
                for (Enumeration<Object> o = noun.getSCHArguments().elements(); o.hasMoreElements(); ) {
                    argument = o.nextElement();
                    if (argument instanceof ValidNoun) {
                        createNoun((ValidNoun) argument, charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        plural = true;
                    }
                    else if (argument instanceof NounName) {
                        ValidNoun n = storyInterpreter.findNounDefinition(TimelineID.REALITY, (NounName)argument);
                        createNoun(n, charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                    }
                    else if (argument instanceof SCHArguments) {
                        ArrayList<SchLexSynt> coord = new ArrayList<SchLexSynt>();
                        boolean firstMemeber = true;
                        for (Enumeration<Object> o2 = ((SCHArguments)argument).elements(); o2.hasMoreElements(); ) {
                            Object groupMembers = o2.nextElement();
                            if (groupMembers instanceof NounName) {
                                // set the first member of the group to this SchNoun, add the others to a list, and set them to a coord and
                                ValidNoun n = storyInterpreter.findNounDefinition(TimelineID.REALITY, (NounName) groupMembers);

                                // first member of the group
                                if (firstMemeber) {
                                    firstMemeber = false;
                                    plural = false; // unless otherwise
                                    createNoun(n, ((NounName) groupMembers).toRawString(), noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                                }
                                // subsequent members of the group
                                else {
                                    SchNoun child = new SchNoun(n, ((NounName) groupMembers).toRawString(), 1, StoryManager.storyInterpreter, timeline, verbalizerState);
                                    child.setParent(this);
                                    coord.add(child);
                                }
                            }
                        }
                        if (!coord.isEmpty()) {
                            SchFunc and = new SchFunc("and", coord);
                            //and.setParent(this);
                            //children.add(and);
                            setChild(and);
                        }
                    }
                }
            }

            // 2) "type of something", "role of something"
            // Ex. the branch of the tree -> tree's branch; branch -> tree
            // Ex: 'anon_role of something unique(noun4975122:hue(), anon_prop the part of something(noun1896031:feather_3(), anon_noun1503061:bird(CharacterGender.Female)_1)_1)_1'
            // 'anon_character the role of something(CharacterGender.Male, noun998806:dad_3(CharacterGender.Male), Lillian)_1
            // "the bowls's water; bowl of the water
            // the ValidNoun is bowl; NounName is water
            // create this SchNoun as the ValidNoun. If there is a NounName, we make a SchNoun and set it as the child of ValidNoun
            else if (nounTypeInfo.contains("of something") || nounTypeInfo.contains("for something")) {
                try {
                    ValidNoun arg2;
                    Object argchild;
                    for (Enumeration<Object> o3 = noun.getSCHArguments().elements(); o3.hasMoreElements(); ) {
                        argchild = o3.nextElement();
                        if (argchild instanceof ValidNoun) {
                            createNoun((ValidNoun) argchild, charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        }
                        else if (argchild instanceof NamedNoun) {
                            createNoun(((NamedNoun) argchild).getNoun(), charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        }
                        else if (argchild instanceof NounName) {
                            arg2 = storyInterpreter.findNounDefinition(timeline, (NounName) argchild);
                            SchNoun child = new SchNoun(arg2, ((NounName) argchild).toRawString(), 0, StoryManager.storyInterpreter, timeline, verbalizerState);
                            child.setParent(this);
                            child.setArticle("no-art");
                            child.possessive = true;
                            if ((child.lexeme != null && child.lexeme.equals("interlocutor")) // you
                                    || !child.name.equals("")
                                    || (child.lexeme != null && child.firstPerson)) { // I
                                article = "no-art";
                            }
                            //children.add(child);
                            setChild(child);
                        }
                    }
                } catch (Scheherexception e) {}
            }

            // 3) "every"
            else if (nounTypeInfo.contains("EVERY")) {
                Object arg;
                for (Enumeration<Object> o = noun.getSCHArguments().elements(); o.hasMoreElements(); ) {
                    arg = o.nextElement();
                    if (arg instanceof ValidNoun) {
                        createNoun((ValidNoun) arg, charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        //initNoun((ValidNoun)arg, charName, noun_ind);
                        article = "no-art";
                        SchAttr every = new SchAttr("ATTR", "every");
                        //every.setParent(this);

                        for (SchLexSynt child: children) {
                            // every corner (no-art) of the narrator's (def, II) apartment (def, II)
                            if (child instanceof SchNoun) {
                                ((SchNoun)child).index = 1;
                                ((SchNoun)child).article = "def";
                            }
                        }
                        //children.add(every);
                        setChild(every);
                    }
                }
            }

            // 4) "generic"
            else if (nounTypeInfo.contains("GENERIC")) {
                Object arg;
                for (Enumeration<Object> o = noun.getSCHArguments().elements(); o.hasMoreElements(); ) {
                    arg = o.nextElement();
                    if (arg instanceof ValidNoun) {
                        createNoun((ValidNoun) arg, charName, noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        article = "def";
                    }
                    else if (arg instanceof NamedNoun) {
                        createNoun(((NamedNoun) arg).getNoun(), ((NamedNoun) arg).getName().toString(), noun_ind, StoryManager.storyInterpreter, timeline, verbalizerState);
                        article = "def";
                    }
                }
            }

            // 5) "abstract" // TODO: need example of this and need to clean it up
            else if (nounTypeInfo.contains("abstract")) {
                ValidNoun arg1, arg2;
                Object arg;
                for (Enumeration<Object> o = noun.getSCHArguments().elements(); o.hasMoreElements(); ) {
                    arg = o.nextElement();
                    if (arg instanceof ValidNoun) {
                        initNoun((ValidNoun)arg, charName, noun_ind);
                    }
                    if (arg instanceof NounName) {
                        arg2 = StoryManager.storyInterpreter.findNounDefinition(TimelineID.REALITY, (NounName)arg);
                        SchNoun child = new SchNoun(arg2, ((NounName)arg).toRawString(), 0, StoryManager.storyInterpreter, timeline, verbalizerState);
                        //child.setParent(this);
                        child.setArticle("no-art");
                        child.possessive = true;
                        if ((child.lexeme != null && child.lexeme.equals("interlocutor")) || !child.name.equals("")) {
                            article = "no-art";}

                        //if (index == 0)	{children.add(child);}
                        //else {children.add(child);}
                        setChild(child);
                    }
                }
            }

            else if (nounTypeInfo.contains("doing some action")) { // gerund
                Object arg;
                for (Enumeration<Object> o = noun.getSCHArguments().elements(); o.hasMoreElements(); ) {
                    arg = o.nextElement();
                    if (arg instanceof ValidNoun) {
                        initNoun((ValidNoun)arg, charName, noun_ind);
                        article = "indef";
                    }
                    else if (arg instanceof ValidAction) {
                        SchVerb gerund = new SchVerb((ValidAction)arg, 1, StoryManager.storyInterpreter, timeline, verbalizerState);
                        //gerund.setParent(this);
                        //children.add(gerund);
                        setChild(gerund);
                    }
                }
            }

            // 6) "Root level" (not sure what to do here, but it seems that if we do nothing, it will be handled elsewhere
            else if (nounTypeInfo.contains("Root Level")) {}

        } catch (Scheherexception e) {e.printStackTrace();}

    }

    public SchNoun(String word, int i) {
        lexeme = word;
        name = "";
        pluralForm = "";
        index = i;
        flavor = null;
        pronoun = "";
        article = "def";
        children = new ArrayList<SchLexSynt>();
        firstPerson = false;
        offset = "";
        properties = new ArrayList<String>();
    }

    /******************************************************************************************************************/


    /**
     * If this is a direct speech or noun is narratand
     * change to 'I'
     * @param first
     */
    public void setFirstPerson(boolean first) {
        firstPerson = first;
        origFirstPerson = first;
    }

    public void setParent(SchLexSynt p) {parent = p;}
    public SchLexSynt getParent() {return parent;}

    /**
     *
     * @param pro "pro": personal pronoun; "poss": possessive pronoun; "rel": reflectivepronoun
     */
    public void setPronoun(String pro) {
        pronoun = pro;
        origPronoun = pro;
    }

    /**
     *
     * @param art
     */
    public void setArticle(String art) {
        article = art;
    }

    /**
     *
     * @param i
     */
    public void setIndex(int i) {
        this.index = i;
    }

    /**
     * Sets the elements this node governs
     * @param child
     */
    public void setChild(SchLexSynt child){
        this.children.add(child);
    }

    public void setGender(CharacterGender g) {gender = g;}

    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

    /**
     *
     * @param coref
     */
    public void setCoreference(boolean coref) {
        coreference = coref;
    }

    public void setProperty(String prop) {if (!properties.contains(prop)) {properties.add(prop);}}


    /******************************************************************************************************************/

    /**
     * Determines if this noun is the speaker or narrator and if we should use 'I' or 'me'
     * if noun is narrator and rel is I: lexeme = I  (index 0)
     * if noun is narrator and rel is II: lexeme = II (index 1)
     * @param speaker_type
     * @param speaker_name
     */
    public void setSpeaker(String speaker_type, String speaker_name, boolean possessive) {
        // if the child noun is of the same type as the speaker noun
        if (speaker_type.equals(lexeme)) {

            // if the speaker noun has a name, we check if the child noun also has the same name
            if (speaker_name != "" && speaker_name.equals(name)) {
                if (possessive) {
                    setFirstPerson(true);
                    setPronoun("me");
                    }
                if (index == 0) { // if noun is narrator and rel is I: lexeme = I  (index 0)
                    setFirstPerson(true); }
                else if (index == 1) { // if noun is narrator and rel is II: lexeme = II (index 1)
                    setPronoun("me"); }
            }

            else { // no speaker name
                if (possessive) {   // my
                    setFirstPerson(true);
                    setPronoun("me"); }
                if (index == 0) { // if noun is narrator and rel is I: lexeme = I  (index 0)
                    setFirstPerson(true); }
                else if (index == 1) {  // if noun is narrator and rel is II: lexeme = II (index 1)
                    setPronoun("me"); }
            }
        }
    }
    public void setInterlocutor(String speaker_type, String speaker_name, boolean possessive) {
        // if the child is a narrator character and not the speaker
        if (lexeme.equals("narrator") && speaker_type != "narrator") {
            article = "no-art";
            setPronoun("you");
            plural = true;
        }
    }

    /**
     *
     * @return
     */
    public String getArticle() {
        if (article != null) {return article;}
        return "def";
    }

    /**
     *
     * @return
     */
    public String getPerson() {
        if (pronoun.equals("me") || firstPerson) {
            return "1st";
        }
        else if (pronoun.equals("you")) {
            return "2nd";
        }
        return "";
    }

    /**
     *
     * @return
     * @throws Scheherexception
     */
	public String getLexeme() throws Scheherexception {
        if (lexeme != null) {
            // "you" must presuppose everything. Realpro person=2nd not working for some reason
            if (lexeme.equals("interlocutor") || pronoun.equals("you")) {
                article = "no-art";
                setPronoun("you");
                plural = true;
                return "you";
            }

            // if they have a name
            if (!name.equals("")) {
                proper = true;
                return name.replace("$", "").replace("@", "");
            }



            // there is a special version of the plural version of this word
            // sometimes plural is abstract props
            if (plural && pluralForm != null && !pluralForm.equals("") && !pluralForm.contains("abstract") && !pluralForm.contains("unique")
                    && !pluralForm.contains("members") && !pluralForm.contains("something")) {
                plural = false; // need to set to false or else realpro will realize "children" (pl) as "childrens"
                return pluralForm;
            }


            // TODO:
            //if (lexeme.equals("narrator")) {
            //    article = "no-art";
            //    setPronoun("me");
            //    plural = false;
            //    return "I";
            //}
            return lexeme;
        }
        return "";
    }

    /**
     *
     * @return
     */
	public String getRelation() {
        if (firstPerson == true && possessive == true) {return "attr";} // to eliminate my's
		if (this.index == 0) {return "I";}		
		else if (this.index == 1) {return "II";}
		else if (this.index == 2) {return "III";}
		else return "IV";
	}

    /**
     *
     * @return
     */
	public String getGender() {
		if (this.gender == null) {return "neut";}		
		else if (this.gender.equals(CharacterGender.Female)) {return "fem";}
		else if (this.gender.equals(CharacterGender.Male)) {return "masc";}
		else return "neut";
	}

    /**
     *
     * @return
     */
	public String getPlural() {
		if (this.plural) {return "pl";}
		return "sg";
	}

    /**
     *
     * @return
     */
	public String getNounClass() {
		if (this.proper == false) {return "common_noun";}
		else if (this.proper == true) {
            this.article = "no-art";
            return "proper_noun";}
		else return "common_noun";
	}

    /**
     *
     * @return
     */
    public ArrayList<SchLexSynt> getChildren(){
        return children;
    }

    public String getOffset() {return offset;}
    /**
     *
     * @return
     */
    public boolean isDirectSpeech() {
        if (lexeme != null && this.lexeme.toString().equals("DIRECT*SPEECH")) { return true;}
        else {return false;}
    }

    // used for reseting nodes
    public String getOrigPronoun() {return origPronoun;}
    public boolean getOrigFirstPerson() {return origFirstPerson;}
    public String getOrigLexeme() {return origLexeme;}


    /******************************************************************************************************************/

    /**
     *
     * @return
     */
	public Map<String,String> getFeatures(){
		HashMap<String, String> features = new HashMap<String, String>();
		try {
            // optional pronoun features
            if (coreference) { // automatic in RealPro, he, she, him, her, it
                features.put("class", "common_noun");
                features.put("pro", "pro");
            }

            // the usual way of defining dsynts
            if (firstPerson == false) {
                features.put("lexeme", getLexeme());
                features.put("class", getNounClass());

                features.put("article", getArticle());
                if (!getPerson().equals("")) {features.put("person", getPerson());}
                if (offset.length() > 0) {features.put("wn_offset", offset);}
            }

            // changing the noun to first person, can also handle myself
            // rel I -> I
            // else -> me
            if (firstPerson == true || pronoun.equals("me")) {
                features.put("lexeme", getLexeme());
                features.put("class", "common_noun");
                features.put("pro", "pro");
                features.put("article", "no-art");
            }

            // brother is being coref'ed
            if (possessive && ((pronoun.equals("you") || pronoun.equals("me")) || coreference)) {
                features.put("pro", "poss");
            }

            // all nouns can have a ref feature which we will use as the lexeme
            // realpro will automatically realize as himself, herself, itself, themselves
            if (!name.equals("")) {features.put("ref", name);}
            else {features.put("ref", nounNameID);}


		} catch (Scheherexception e) {
            e.printStackTrace();
		}

        for (int i = 0; i < properties.size(); i++) {
            features.put("property_"+i, properties.get(i));
        }

        features.put("person", getPerson());
		features.put("number", getPlural());
		features.put("gender", getGender());
		features.put("rel", getRelation());
		return features;
	}
	
	/**********************************************************************/


}