A wolf saw a lamb straying away from the flock of the lamb.  

The wolf began to be obligated to rationalize about killing the lamb because the lamb was helpless.  

The wolf said to the lamb that the lamb had long ago insulted him.  

The lamb said to the wolf that it wasn't possible for it to have long ago insulted him because it was young.  

The wolf said to the lamb that he owned a pasture and said to the lamb that the lamb had been feeding on the pasture.  

The lamb said to the wolf that it wasn't possible it have been feeding on the pasture because the lamb had not tasted some grass.  

The wolf said to the lamb that he owned some spring water and said to the lamb that the lamb had been drinking the spring water.  

The lamb said to the wolf that it wasn't true that it had been drinking the spring water.  

The wolf said that he planned to later eat.  

The wolf ate the lamb.  

