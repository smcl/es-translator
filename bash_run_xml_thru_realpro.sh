#!/bin/bash
#
# run_xml_thru_realpro.sh
# Author: Stephanie Lukin
# Purpose: This script will automate the process of passing each xml file generated
# 	by Scheherazade MainEST through Real Pro. This script requires the dataset variable to be
#   set to 'dev', 'train', or 'test'
#
# This script assumes the following strict program structure:
# - RealPro-2.3/ (in theory, we can move this anywhere, but I put it here for convenience)
# - es-translator/ (git repo)
	# - run_xml_thru_realpro.sh
		# - data/
			# - xml/ (xml output produced by EST Basic)
				# - dev/ 
				# - train/ 
				# - test/ 
			# - realpro/ (realizations of xml that will be output by this script)
				# - dev/ 
				# - train/ 
				# - test/ 
#				
# NOTES: IF USING RUN.BAT, NEED TO MODIFY REALPRO RUN.BAT SCRIPT TO NOT PAUSE
# 	--> change last line to 'REM pause' or remove 'pause'
# 	otherwise, specify run.unix and it should be fine



# TODO: we need to set this to "./run.bat" (windows) or "./run.unix" (unix)
RUNREALPRO="./run.bat"

# TODO: if the realpro directory is not at the same level as this repo, then set the complete path here
REALPRODIR=".."

##########################################################

DATASET=$1
AGG=$2

# set the root project directory, the directory this script is executed in 
ROOTDIR=$PWD/
echo ROOTDIR is $ROOTDIR

# need to check for window's cygwin and remove from path because it causes problems
# systems not using cygwin should not be effected, i.e. macs and linux machines
if [ ${ROOTDIR:0:9}="/cygdrive" ] 
then
	ROOTDIR=${ROOTDIR:10:1}:/${ROOTDIR:12}
	echo NEW ROOTDIR is $ROOTDIR
fi

# data path to xml output 
XMLPATH=$ROOTDIR"data/xml"$2"/"$DATASET/
echo XMLdir is $XMLPATH
cd $XMLPATH

# store the xml files
FILES=`ls`

# dir for outputting realpro
OUTPATH=$ROOTDIR"data/realpro"$2"/"$DATASET/

# navigate to where realpro lives and the xml loading program
cd $ROOTDIR
cd $REALPRODIR
cd "RealPro-2.3/sample-applications/Realizing-DSyntS-Loaded-From-XML-File/"

# iterate through all xml files stored
for f in $FILES
do
	echo $f
	
	# need to remove the .xml file extension
	# split the string by the '.xml' and take [1]
	FILENAME=`echo $f | awk '{split($0,a,".xml"); print a[1]}'`".txt"

	# call RealPro and redirect output 
	$RUNREALPRO $XMLPATH$f > $OUTPATH$FILENAME
	
	# trim the RealPro junk from the file and output just the story
	python ../../../es-translator/py_scripts/trimRealPro.py $OUTPATH$FILENAME
done
