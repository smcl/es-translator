There once was a lamb and a family.  The family was a sheep.  The lamb was straying away from the family.  He was helpless.  

A wolf came near the lamb.  

The wolf didn't inexcusably want to kill the lamb.  

The wolf tried to find reason.  

The wolf said that the lamb later grossly insulted the wolf.  

The lamb respectfully bleated that an insult was impossible because the lamb wasn't alive.  

The wolf retorted that the lamb ate some grass, and the wolf began to own it.  

The lamb said that the wolf accusing the lamb of being thieving was impossible because the lamb didn't eat the grass.  

The wolf persisted.  

The wolf began to own a spring and said that the lamb drank from the spring.  

The lamb began to be misused.  

The lamb said that he only drank some milk of a mother.  

The wolf balked and regardless wanted some dinner.  

The wolf sprang up on the lamb and immediately devoured him.  

