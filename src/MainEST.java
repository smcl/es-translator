import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

import net.didion.jwnl.JWNL;

import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.dictionary.Dictionary;
import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.admin.StoryListener;
import story.scheherazade.elements.Noun;
import story.scheherazade.examples.StandardKnowledgeBase;
import story.scheherazade.misc.CharacterType;
import story.scheherazade.verbalizer.PlannerRepository;
import story.scheherazade.verbalizer.Verbalizer;
import story.scheherazade.misc.ActionType;


/**
 * Scheherazade to Personage Transformation Code
 * <br>ES Translator
 * <br>Expressive Story Transducer
 * <br>(Elena Stephanie Translator)
 * <p></p>
 * <b>GENERAL OVERVIEW</b>
 * <p></p>
 * We define our own classes for Nouns, Verbs, Adjectives, Adverbs, and others
 * We extract the relevant information from Scheherazade's representation
 * We represent a sentence in the story as a Lexico-Syntactic unit, a tree-like structure
 * Each Noun, Verb, etc we define as a Lexico-Syntactic unit, containing the relevant information
 * needed to create a dsynts (class, lexeme, number, tense, ...)
 * <p>
 * <b>PROJECT CONTENTS</b>
 * <p></p>
 * data:/
 * <br>All data directories have a dev/, train/, and test/ subdirectory
 * <br>- original/: Aesop's fables and blogs stories
 * <br>- realpro/: our Realpro generated output from our methods
 * <br>- sch/: the Scheherazade generated output
 * <br>- tests/: levenshtein distance and set overlap results
 * <br>- vgl/: vgl files from Scheherazade annotations
 * <br>- xml/: our xml generated output from our methods
 * <br>- assertions/: prolog version of Scheherazade annotations (see doc/searching sig with prolog.rtf for details)
 * <p></p>
 * es-translator/:
 * <br>- run_xml_thru_realpro.sh: given [dev,train,test], this script runs these xml files through realpro and outputs to data/realpro/[dev,train,test]/
 * <p></p>
 * py_scripts/:
 * <br>- buildXml4BLEU.py
 * <br>- compare_output.py: Compares the story text between [sch, realpro], [original, sch], or [original, realpro]
 * <br>- direct_speech_transform.py: Given [dev,train,test], a story (filename) and a direct speech format, transforms a direct speech utterance realized by realpro into different formats of direct speech
 * <br>- extract_fables.py: Extracts the original Aesop's fables from the .vgl files, and creates new files containing only the fables
 * <p></p>
 * jwnl/:
 * <br>- java library for WordNet
 * <p>
 * lib/:
 * <br>- various dependencies
 * <p>
 * doc/:
 * <br>- Javadoc
 * <p>
 * Setup Project
 * <br>- git clone https://<your username>@bitbucket.org/erishes/es-translator.git
 * <br>- enter bitbucket password
 * <p>
 * - In eclipse:
 * <br>- Right click on project > Build path > Configure Build path
 * <br>- Under 'Libraries' tab > 'Add External Jars'
 * <br>- Select 'jwnl/jwnl-1.4_rc3.jar' (add this first)
 * <br>- Select 'lib/scheherazade-0.33.jar'
 * <br>- Select 'lib/pers_rel.jar'
 * <br>- Select 'lib/jverbset-1.2.0-jdk.jar'
 * <p></p>
 * - In IntelliJ:
 * <br>- File > project structure
 * <br>- 'Modules' tab > 'Dependencies' tab
 * <br>- Add (green +) 'jwnl/jwnl-1.4_rc3.jar' (add this first)
 * <br>- Add 'lib/scheherazade-0.33.jar'
 * <br>- Add 'lib/pers_rel.jar'
 * <br>- Add 'lib/jverbset-1.2.0-jdk.jar'
 * <p>
 * Note: If you experience out of memory exceptions when loading the Knowledge Base, increase memory size
 * - In IntelliJ: Run > Edit configurations > VM options: -Xmx512M
 * <p></p>
 * <b>PROGRAM FLOW</b>
 * <br></br>Start in MainEST.java
 * We iterate through all the vgl files in the given directory
 * For each vgl file, we run processVGL() then toRealPro()
 * processVGL extracts the information from the vgl file and transforms it into our own data structures
 * toRealPro iterates through our data structures and applies rules to output a dsynt format
 * <P></P>
 * processVGL -> loadVGL()
 * loadVGL contains code mostly written by David. It 'hears' the story and extracts story points. We then iterate through each story point and process each one -> dislpayAction
 *<P></P>
 * In displayAction(), we can verbalize the story point. They can be actions or interpretations
 * <p></p>
 * If it is action, we process it with returnAction().  Each 'action' is a new sentence
 * <p></p>
 * returnAction()
 * <br>The first thing we do is create a SchVerb object from the action. There are different types of actions (AssignedAction, AssignedCondition, ValidAction)
 * <p></p>
 * SchVerb
 * <br>A variety of properties that we use for creating the dsynts
 * <p></p>
 * Then we look for modifiers in returnAction()
 * Adverbs, function words, prepositions, nouns, adjectives (again, rules for extracting the information)
 * We set all modifiers to be children of the verb
 * This is recursive
 * <p></p>
 * toRealPro()
 * <br>saveDsynt() goes through the SchVerbs we have and runs getFeatures() to extract the appropriate information for the dsynts. Each SchObject has a unique get features method which writes to a map data structure
 * <p></p>
 * SchVerb is very simple
 * SchNoun is more complicated, but I’ve been adding things like direct speech, first and second person so my code is still in development
 * <p></p>
 * Then saveDsynt() will convert the map into xml format and output it
 */
public class MainEST {
    private static StoryListener storyListener;
    private static Verbalizer verbalizer;
    private static Parameters parameters;
    private static StoryManager story;

    /**
     * Initializes the VGL reader and verbalizer, and WordNet dictionary
     *
     * @throws Scheherexception
     * @throws IOException
     */
    public static void initWordNet() throws Scheherexception, IOException {
        System.out.println("Initializing Wordnet...");
        try { // initialize WordNet
            JWNL.initialize(new FileInputStream("jwnl/file_properties.xml"));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
        System.out.println("Wordnet loaded.");
    }

    public static void initKB() throws Scheherexception, IOException {
        // initializes VGL reader and verbalizer
        System.out.println("Loading KB...");
        final StandardKnowledgeBase kb = new StandardKnowledgeBase(null);
        storyListener = kb.getStartupStoryListener();
        final PlannerRepository planners = kb.getPlannerRepository();
        verbalizer = new Verbalizer(planners);
        System.out.println("Done. Loading files...");
    }

    /**
     * Processes vgl from [dev,train,test] dataset and outputs all data to respective subdirectory in data/
     * Creates an xml file, scheherazade file, SIG event pairs
     * Can also tell the story from different characters' perspectives
     *
     * @param storyListener
     * @param verbalizer
     * @param dataset [dev,train,test]
     * @throws Scheherexception
     * @throws IOException
     */
    public static void runEstBasic(String vgl_path, StoryListener storyListener, Verbalizer verbalizer, String dataset) throws Scheherexception, IOException, Exception {
        // hack for determining if we're running on a windows or mac
        String directory = new File("").getAbsolutePath();
        // check to see whose computer we are on
        if (directory.contains("es-translator\\src")) {
            // then it's a window's machine
            directory = directory.substring(0, directory.indexOf("es-translator\\src")) + "es-translator";
        }

        // Create string to store each directory we'll use
        String data_dir = directory + "/data/";
        String vgl_dir = data_dir + "vgl/"+dataset+"/";
        String xml_dir = data_dir + "xml/"+dataset+"/";
        String textplan_dir = data_dir + "textplans/"+dataset+"/";
        String sch_dir = data_dir + "sch/"+dataset+"/"; // scheherazade realization
        String story_dir = data_dir + "original/"+dataset+"/"; // original story
        String pairs_dir = data_dir + "eventpairs/"+dataset+"/"; // SIG event pairs

        // open the vgl dev, train, or test directory
        // open the vgl dev, train, or test directory
        File[] vgl_set;

        // only a single file
        if (vgl_path != null) {
            vgl_set = new File[1];
            vgl_set[0] = new File(vgl_path);
        }
        // an entire dataset
        else {
            vgl_set = new File(vgl_dir).listFiles();
        }
        int storyNumber = 1;

        String file_name, xml_file, sch_file, story_file, original_story, textplan_file;
        Scanner fileScanner;
        FileWriter fw;

        // iterate through all the files in the dataset
        //StoryManager story;
        for (File vgl_file : vgl_set) {
            if (vgl_file.isFile() && vgl_file.getName().endsWith(".vgl")) {
                file_name = vgl_file.getName().substring(0, vgl_file.getName().indexOf(".vgl"));
                xml_file = xml_dir +""+ file_name + parameters.toString()+ ".xml";
                textplan_file = textplan_dir +"textplans_"+ file_name + parameters.toString()+ ".xml";
                sch_file = sch_dir +""+ file_name+ ".txt";

                // get the original story from the .vgl file
                boolean getStory = false;
                original_story = "";
                String line = "";
                try {
                    fileScanner = new Scanner(vgl_file);
                    while (fileScanner.hasNext()) {
                        line = fileScanner.nextLine();
                        if (getStory)
                            original_story += line + " ";
                        if (line.contains("---SOURCE STORY BELOW---"))
                            getStory = true;
                    }
                } catch (IOException e ) {System.out.println("Could not process original story");}

                // process this .vgl story
                System.out.println("====== Story "+storyNumber+": "+vgl_file+" =======");
                story = new StoryManager(storyListener, verbalizer, file_name, original_story, vgl_file.toString());
                story.setParameters(parameters);
                story.setFiles(vgl_file.getAbsolutePath(), xml_file, sch_file, textplan_file);

                String assertionFile = data_dir + "assertions/" + dataset + "/assertions-" + file_name + ".vgl.prolog";
                String prologFile = data_dir + "prolog/" + dataset + "/" + file_name + ".emotions";
                story.setAssertions(assertionFile, prologFile);

                story.processVGL();

                // first need to run through without 3rd pov
                story.toRealPro();

                // handle POV here
                if (parameters.getNarr_pov() == 1) {
                    // tell the story from different character perspectives
                    // iterate through all characters and modify the dsynts to reflect their POV
                    String characterName;
                    for (SchNoun character: story.storyCharacters) {
                        characterName = character.getLexeme().replace(" ", "");
                        if (!characterName.equals(parameters.getNarr_foc())) {continue;}
                        System.out.println("Writing story for character: " + characterName);
                        story.toRealPro(xml_dir + "" + file_name + parameters.toString() + ".xml",
                                textplan_dir + "" + file_name + parameters.toString() + ".xml",
                                characterName, character);
                    }
                }
                // save original or modified dsynts
                story.saveOutput();

                try {
                    fw = new FileWriter(new File(pairs_dir +""+ file_name + ".txt"));
                    fw.write(story.storyGraph.toString());
                    fw.close();
                } catch (IOException e ) {System.out.println("Could not write event pairs");}

                storyNumber+=1;
            }
        }
        System.out.println("All " + Integer.toString(storyNumber-1) + " stories analysed");
    }


    /**
     * Processes a single file and outputs it to personage domain
     * if vgl_path = null; then we are running the entire dataset
     * @param storyListener
     * @param verbalizer
     * @param vgl_path
     * @param personage_path
     * @throws Scheherexception
     * @throws IOException
     */
    public static void runPersonage(String vgl_path, StoryListener storyListener, Verbalizer verbalizer, String dataset, String personage_path) throws Scheherexception, IOException, Exception {
        // hack for determining if we're running on a windows or mac
        String directory = new File("").getAbsolutePath();
        // check to see whose computer we are on
        if (directory.contains("es-translator\\src")) {
            // then it's a window's machine
            directory = directory.substring(0, directory.indexOf("es-translator\\src")) + "es-translator";
        }

        // Create string to store each directory we'll use
        String data_dir = directory + "/data/";
        String vgl_dir = data_dir + "vgl/"+dataset+"/";

        // open the vgl dev, train, or test directory
        File[] vgl_set;

        // only a single file
        if (vgl_path != null) {
            vgl_set = new File[1];
            if (!vgl_path.contains(".vgl")) {
                vgl_path = vgl_path + ".vgl";
            }
            vgl_set[0] = new File(vgl_path);
        }
        // an entire dataset
        else {
            vgl_set = new File(vgl_dir).listFiles();
        }
        int storyNumber = 1;

        // iterate through all the files in the dataset
        StoryManager story;
        for (File vgl_file : vgl_set) {
            if (vgl_file.isFile() && vgl_file.getName().endsWith(".vgl")) {
                String file_name = vgl_file.getName().substring(0, vgl_file.getName().indexOf(".vgl"));

                // get the original story from the .vgl file
                String original_story = "";
                boolean getStory = false;
                String line = "";
                try {
                    Scanner fileScanner = new Scanner(vgl_file);
                    while (fileScanner.hasNext()) {
                        line = fileScanner.nextLine();
                        if (getStory)
                            original_story += line + " ";
                        if (line.contains("---SOURCE STORY BELOW---"))
                            getStory = true;
                    }
                } catch (IOException e ) {System.out.println("Could not process original story");}

                // process this .vgl story
                System.out.println("====== Story "+storyNumber+": "+vgl_file+" =======");
                story = new StoryManager(storyListener, verbalizer, file_name, original_story, personage_path);
                story.setParameters(parameters);
                story.setFiles(vgl_file.getAbsolutePath(), "", "", ""); // don't need xml_file or sch_file

                String assertionFile = data_dir + "assertions/" + dataset + "/assertions-" + file_name + ".vgl.prolog";
                String prologFile = data_dir + "prolog/" + dataset + "/" + file_name + ".emotions";
                story.setAssertions(assertionFile, prologFile);

                story.processVGL();
                story.toPersonage(true);

                // have text plans, dsynts that is what Personage requries as input
                // instead, can we send the text plans and dsynts to our own DSSTransformer?
                // create new objects to hold them, instead of writing them to output, but that can wait

                // tell the story from different character perspectives
                // iterate through all characters and modify the dsynts to reflect their POV
                if (parameters.getNarr_pov() == 1) {
                    for (SchNoun character: story.storyCharacters) {
                        String characterName = character.getLexeme().replace(" ", "");
                        if (characterName.equals(parameters.getNarr_foc()) || character.origLexeme.equals(parameters.getNarr_foc())) {
                            System.out.println(characterName);
                            story.toPersonage(characterName, character, true);
                        }
                    }
                }
                storyNumber+=1;
            }
        }
        System.out.println("All " + Integer.toString(storyNumber-1) + " stories analysed");
    }

    //*************************************************************************************/

    public static void runEST(int main, String storyPrefix, String dataset, String storyName, String personagePath)
            throws Scheherexception, IOException, Exception {

        String directory = new File("").getAbsolutePath();
        String storyFile = "";

        if (main == 2 || main == 3) {
            if (storyPrefix != "") {
                File aDirectory = new File(directory + "/data/vgl/" + dataset + "/");
                File[] filesInDir = aDirectory.listFiles();
                for (int i = 0; i < filesInDir.length; i++) {
                    if (filesInDir[i].getName().startsWith(storyPrefix)) {
                        storyName = filesInDir[i].getName();
                        storyFile = directory + "/data/vgl/" + dataset + "/" + storyName;
                        break;
                    }
                }
                if (storyName.equals("")) {
                    System.out.println("Can't load story with prefix " + storyPrefix + " in dataset " + dataset);
                    return;
                }
            } else if (storyName != ""){
                storyFile = directory + "/data/vgl/" + dataset + "/" + storyName;
            }

        }

        System.out.println("Story: " + storyFile);
        initKB();

        // NOTE: only one of these will be run at a time
        if (main == 1) {
            runEstBasic(null, storyListener, verbalizer, dataset);
        }
        else if (main == 2) {
            runEstBasic(storyFile, storyListener, verbalizer, dataset);
        }
        else if (main == 3) {
            final String DOMAIN = "scheherazade";
            String path = personagePath + "/domains/" + DOMAIN;
            runPersonage(storyFile, storyListener, verbalizer, dataset, path);
        }
        else if (main == 4) {
            final String DOMAIN = "scheherazade";
            String path = personagePath + "/domains/" + DOMAIN;
            runPersonage(null, storyListener, verbalizer, dataset, path);
        }

        System.out.println("Finished on \n\tStory: " + storyName + "\n\tDataset: " + dataset +
                "\n\tParameters: " + parameters.toString());
    }

    //*************************************************************************************/

    /**
     * Main class: User can
     * 1) run entire dataset for EST Basic output
     * 2) run single story for EST Basic output
     * 3) run single story for personage
     * 4) run entire dataset for personage
     * @param args
     * @throws Scheherexception
     * @throws IOException
     */
	public static void main(String[] args) throws Scheherexception, IOException, Exception {
        initWordNet(); // initialize wordnet

        final String DEV = "dev";
        final String TRAIN = "train";
        final String TEST = "test";
        final String BLOGS = "blogs";
        final String FABLES = "fables";

        // preset parameter values
        int none = 0;
        int med = 50;
        int high = 100;

        // TODO: set the path to personage if applicable
        String personagePath = "C:/Users/Stephanie/personage4est";

        // TODO: set the dataset you're using here
        // You may define your own as long as the corresponding
        // directories exist in data/xml /original /realpro
        String dataset = FABLES;

        //TODO: selection function
        // 1: run entire dataset for EST Basic output
        // 2: run single story for EST Basic output
        // 3: run single story for personage
        // 4: run entire dataset for personage
        int main = 2;

        // TODO: set the story here, if running a single story
        // You may use the story prefix, or if your story doesn't have a prefix
        // leave storyPrefix blank and set storyName
        String storyPrefix = "";
        String storyName = "A115-TheFoxandTheCrow.vgl";

        // set to false if we are not overgenerating. Basic EST does not overgenerate!
        // Overgenerate requries Personage hook in. Overgenerating will run for a single
        // story through all 18 combinations of narrative parameters defined below..
        boolean isOvergenerating = false;

        // TODO: list the characters from whose perspectives to tell.
        // If this is empty, will default to run in 3rd person.
        String[] characters = {"fox", "crow"};

        // main for single runs
        if (!isOvergenerating) {
            parameters = new Parameters();
            // Default to run in 3rd person
            if (characters.length == 0) {
                parameters.setNarr_pov(3);
                parameters.setNarr_directSpeech(none);
                parameters.setSent_deagg(none);
                runEST(main, storyPrefix, dataset, storyName, personagePath);
            }
            // Iterate through defined characters to retell in their POV.
            for (String character : characters) {
                parameters.setNarr_pov(1);
                parameters.setNarr_foc(character);
                parameters.setNarr_directSpeech(none);
                parameters.setSent_deagg(none);
                runEST(main, storyPrefix, dataset, storyName, personagePath);
            }
        }

        // main loop for overgenerating. Ignore on EST Basic.
        // setCombination(i) offers an easy way to iterate through different parameters.
        else {
            boolean directSpeech = true;
            for (int i = 0; i < 18; i++) {
                parameters = new Parameters();
                if (i < 9) { // 1st person
                    if (!directSpeech && i >= 3 & i <= 8) {continue;}
                    for (String character : characters) {
                        parameters.setNarr_foc(character);
                        parameters.setCombination(i); // 0 - 17
                        runEST(main, storyPrefix, dataset, storyName, personagePath);
                    }
                } else {
                    if (!directSpeech && i >= 12 & i <= 17) {continue;}
                    parameters.setCombination(i); // 0 - 17
                    runEST(main, storyPrefix, dataset, storyName, personagePath);
                }
            }
        }

    }





}
