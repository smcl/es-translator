import story.scheherazade.examples.StandardKnowledgeBase;

import java.io.File;
import java.util.Iterator;

import story.scheherazade.elements.*;
import story.scheherazade.misc.*;
import story.scheherazade.parameters.*;
import story.scheherazade.admin.*;
import story.scheherazade.verbalizer.*;
import story.scheherazade.exceptions.*;
import story.scheherazade.prologuer.*;
import story.dbcore.exceptions.*;
import story.scheherazade.virgil.VirgilFile;
import story.scheherazade.virgil.VisibleStates;

/**
 * Demo of the Scheherazade API. 
 *
 * by David K. Elson
 * 11/23/2010
 *
 * Copyright 2010 David K. Elson
 */
public class ScheherazadeDemo {

    private StoryListener listener;
    private PlannerRepository planners;
    private TimelineID declarationTimeline;
    private StoryInterpreter storyInterpreter;
    private Verbalizer verbalizer;
    private VerbalizerState verbalizerState;

    public ScheherazadeDemo () throws Scheherexception  { 

	////// SETUP //////

	// Load the knowledge base.
	final StandardKnowledgeBase kb = new StandardKnowledgeBase(null);
    System.out.println("Done.");

	// Get a StoryListener from the loaded KB.  This is the
	// interface to commanding Scheherazade.
	final StoryListener storyListener = kb.getStartupStoryListener();
    System.out.println("Done.");

	// A StoryInterpreter is a helper class that makes commanding
	// Scheherazade easier.  The first Javadoc you should read to
	// learn the Scheherazade API is StoryInterpreter.
	storyInterpreter = new StoryInterpreter(storyListener);
    System.out.println("Done.");

	// Have Scheherazade initialize a new story.  Give it a name.
	storyInterpreter.initializeNarrative(new NarrativeID("My Great Story"));

	// Get the generation planners provided by the KB.  
	planners = kb.getPlannerRepository();

	// Instantiate a new Verbalizer.  Verbalizer is the module
	// that generates textual renderings of parts of story graphs.
	verbalizer = new Verbalizer(planners);

	// All done.
	System.out.println("Done.");



    //  Required for saving to vgl //
    StoryTelling startupTelling = storyListener.getTelling();
    VisibleStates vs = new VisibleStates();
    StateTimeSet sts = new StateTimeSet();
    sts.add(new StateTime(StateTimeType.Present, -1));
    sts.add(new StateTime(StateTimeType.Present, 0));
    sts.add(new StateTime(StateTimeType.Present, 1));

    //// ADDING NEW CONTENT ////

    /** Let's manually add a new action.
     * I'll assume we're okay with using the verb frames available
     * in the Standard KB.  But new verb frames can be declared
     * with declareAction; new types of conditions, nouns and
     * modifiers can similarly be added.
     */

    // Here is a large list of all available actions.
    ActionTypeSet allActionFrames = storyInterpreter.getDeclaredActions(TimelineID.REALITY, true);
    System.err.println("There are "+allActionFrames.size()+" action predicates available in the knowledge base.");

    // We can list them if we want.  One of them should be the following:
    ActionType somethingSwoops = new ActionType("verb2064358_swoop_something_swoops");

    // The name of the action means:
    // "verb" + (WordNet offset, via VerbNet) + "_" + (verb head) + (abstract reading of frame in present simple tense)

    // Note that some action types are placeholders, used only to
    // create a hierarchy of actions; they cannot be instantiated.

    // So what does this action take?
    StoryAssertion actionParameters = storyInterpreter.getActionParameters(TimelineID.REALITY, somethingSwoops);

    // (This command will fail if the action type we constructed
    // ourselves does not match one in the knowledge base.)  The
    // result, StoryAssertion, is a list of required arguments.
    // There should only be one for this frame, the thing
    // swooping.
    System.out.println("The action "+somethingSwoops+" calls for the following arguments: "+actionParameters.render());
    ValidNounParameter arg0 = (ValidNounParameter)actionParameters.getParameter(0);
    System.out.println("The only required argument, a noun for the semantic role "+arg0.getRole()+", accepts the following noun \"flavors\": "+arg0.getAllowedFlavors().render());

    // Well, in order to fill that argument, we need a Noun.
    // Nouns are themselves predicates in Scheherazade.  Some take
    // significant arguments (e.g., "the father of the bride" is
    // father(bride()) with "bride" the argument of "father").
    // Characters also take Gender arguments.  So in order to have
    // something swoop, let's define that something in the story.
    // But to keep things simple, let's use a generic noun.

    NounName genericCharacter = storyInterpreter.defineNoun(TimelineID.REALITY, new Noun(CharacterType.Universal));

    // see ExternalKB.java for arguments
    NounName myCharacter = storyInterpreter.defineNoun(TimelineID.REALITY, new Noun(new CharacterType("noun2129165:lion")));
    System.out.println("I just told the story about a generic character called "+genericCharacter.toString()+", or \"some character.\"");

    // OK.  We have our noun. We can roll the predicate and argument structure together like this:
    Action newSwoopAction = new Action(somethingSwoops, new SCHArguments(myCharacter), 1, PredicateMode.ActionAssert);

    // When do we want the action to take place?  How about Time 2
    // to Time 8, since we have that TransitionTime already.
    // Assign it to the story like this:
    StateTime time2 = new StateTime(StateTimeType.Present, 2);
    StateTime time8 = new StateTime(StateTimeType.Present, 8);
    TransitionTime time2toTime8 = new TransitionTime(time2, time8);
    AssignedAction assignedAction = storyInterpreter.assignAction(TimelineID.REALITY, time2toTime8, newSwoopAction);

    // This will throw an exception if anything goes wrong,
    // including if the provided arguments did not satisfy the
    // parameters of the action type.  (There is a
    // behind-the-scenes validation step.)  But if we are here, it
    // means the assignment went well, and it returned an
    // AssignedAction object as a "receipt."

    System.out.println("I successfully assigned the following action manually:\n");
    displayAction(assignedAction);


    // OUTPUT TO VGL
    StoryTelling storyTelling = storyListener.getTelling();
    AssignedActionSet actionSet = storyInterpreter.getAllActions(TimelineID.REALITY);
    for (Iterator<AssignedAction> i = actionSet.iterator(); i.hasNext();) {
        // print each point
        AssignedAction action = i.next();
        System.out.println("    "+action.render());
        if (!sts.contains(action.getTime().time1()) && !action.getTime().time1().equals(StateTime.UNBOUNDED)) {
            sts.add(action.getTime().time1());}
        if (!sts.contains(action.getTime().time2()) && !action.getTime().time2().equals(StateTime.UNBOUNDED)) {
            sts.add(action.getTime().time2());}
    }

    vs.setStates(TimelineID.REALITY, sts);
    String UID = "A102";
    String source = "Story From File";
    String story = "Story text here";
    VirgilFile virgilFile = new VirgilFile(storyTelling, startupTelling.size(), vs, 835742799, UID, source, story);
    virgilFile.save(new File("test_vgl.vgl"));





    //// GENERATION ////

    /**
     * One thing we can do off the bat is generate a random story.
     * This will randomly chooose predicates and then fill them
     * out with random arguments.  This function takes 3
     * arguments: the timeline in which new content should be
     * generated (we'll use the main one, REALITY); the number of
     * predicates we shoudl generate, and language model we should
     * use to guide the selection of predicates (in this case a
     * baseline with equally distributed probability mass).
     */

    // This generates a list of instructions (Story Points)...
	StoryTelling randomStory = storyInterpreter.generateRandomNarrative(TimelineID.REALITY, 15, new LanguageModel(true));

	// This executes those instructions.
	storyListener.hear(randomStory);

	//// READING AND VERBALIZING THE GRAPH ////
	AssignedActionSet allActions = storyInterpreter.getAllActions(TimelineID.REALITY);
	System.out.println("Random story has "+allActions.size()+" actions.\nHere they are: For each action, the transition of two times in which the action occurs, and a reading of the predicate-argument structure. ");
    for (Iterator<AssignedAction> i = allActions.iterator(); i.hasNext(); ) {
        AssignedAction action = i.next();
        System.out.println("    "+action.render());
    }


	/**
	 * So now we have a story.  What does it say? Let's invoke the
	 * "verbalizer" generation module to serialize it into natural
	 * language.  The verbalizer takes a root "discourse plan" --
	 * in this case, "Verbalize the REALITY timeline in Past
	 * Tense."  It also takes an initial state object.
	 */
	DiscoursePlan plan = new DiscoursePlan(new VerbalizeTimeline(TimelineID.REALITY, VerbTense.PastSimple));
	
	verbalizerState = verbalizer.freshState();
	
	// Another variable telling the verbalizer to tell the story
	// in past simple tense.
	verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

	System.out.println("Here is our random story:\n");

	// Run the verbalizer and output the results.
	String result = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, plan, verbalizerState).toString();
	System.out.println(result);



	//// QUERIES ////
	
	// Let's read the graph in another way: with a query.
	// Say we want to say, "shoe me all actions that take place 
	// from Time 2 to Time 8."

	StoryQuery query = new StoryQuery();

	// Queries take two kinds of parameters: those that match
	// elements you want to find, and those that filter out
	// elements according to some criterion.  We keep state by
	// defining variables for use within the query.
	VariableID actionVar = new VariableID("action1");

	// "Find every action in the story and, one  by one, assign each to 
	// the variable actionVar."
	query.addParameter(new AssignedActionParameter(TimelineID.REALITY, actionVar));

	// "Eliminate those actions that do not occur during the span
	// between Time 2 and Time 8."
	time2 = new StateTime(StateTimeType.Present, 2);
	time8 = new StateTime(StateTimeType.Present, 8);
	time2toTime8 = new TransitionTime(time2, time8);
	query.addParameter(new ActionOccursParameter(actionVar, time2toTime8, ActionTemporality.OccursDuring));

	// Run the query and get a result set.
	SCHQueryResultSet queryResults = storyInterpreter.query(query);

	// Iterate through the results.

	System.out.println("The following "+queryResults.numResults()+" actions occur between Time 2 and Time 8:");

	for (Iterator i = queryResults.getQueryResults(); i.hasNext(); ) {

	    SCHQueryResult queryResult = (SCHQueryResult)i.next();
	    // The result has one field for each matching parameter.
	    // Since we had one matching parameter,
	    // AssignedActionParameter, it has one field, and that
	    // contains an AssignedAction.
	    AssignedAction action = (AssignedAction)queryResult.getElement(0);

	    // Print out the query result in both propositional form and English, via the verbalizer.
	    displayAction(action);
	    
	}





	////// UNDO AND REDO //////

	// One feature to highlight is that Scheherazade is a full
	// transactional database.  We can undo the last command (the
	// one that created the Swooping action).
	storyInterpreter.undo();

	// Now everything is as it was a moment ago. We can similarly redo.
	storyInterpreter.redo();


	// And there you have it -- the basic ins and outs of creating
	// a story using the Scheherazade API.  There's lots of more
	// commands available in the StoryInterpreter, and the
	// provided GUI called "Virgil" allows for interactive story
	// annotation using the exact same API.  If you'd like to
	// explore the underlying graph formalism constructed by
	// Scheherazade, uncomment the following line, which will
	// launch a separate graph-browsing GUI.
	
	//storyInterpreter.launchDebugger();


	// Feel free to contact David with any questions, comments or
	// requests.


    }




    
    /**
     * Display an AssignedAction in both raw propositional notation
     * and serialized natural language.
     */
    private void displayAction (AssignedAction action) throws Scheherexception {

	System.out.println("   -- "+action.render());
	
	verbalizerState = verbalizer.freshState();
	verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);
	// "Perception time" is the temporal standpoint from which
	// we view the action; the relationship between the
	// action's time and the perception time affects verb
	// tense and affect.  See Elson and McKeown, "Tense and
	// Aspect Assignment in Narrative Discourse," INLG 2010.
	TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));
	String actionVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(action, action.getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
	System.out.println("       ** \""+actionVerbalization+"\"\n");
    }
	

    public static void main (String[] args) {
	try {
	    new ScheherazadeDemo();
	} catch (Scheherexception s) {
	    s.printStackTrace();
	}
    }

}



