import commands, re, sys, string
import numpy
from scipy import stats

FLAGS = {'ref':'refid', 'tst':'sysid'}

def createXML(path, flag):
	 # to compute BLUE score, convert txt files into xml
    files = commands.getoutput('ls ' + path + '*.txt').split()
    f_names = [f.split('/')[-1][:-4] for f in files]
    with open(path + "BLUE.xml", 'w') as out:
    	out.write("<mteval>\n")
        out.write('<%sset setid="fables" srclang="English" trglang="English" %s="%s">\n' % (flag, FLAGS[flag], path))
    	for name in f_names:
    		with open(path + name + ".txt", 'r') as inp:
				out.write('<doc docid="%s">\n' % name)
				lines = [string.replace(line.strip(), "n't", " not") for line in inp.readlines()]
			
				s = "Printing document with realized DSyntSs (XML format):"
				if s in lines:
					start = lines.index(s)
					lines = [line for line in lines[start + 4:-1]]
				# clear of punctauation and capitalization
				words = [w for l in lines for w in re.findall(r"[a-z]+", l.lower())]
				out.write("<seg id='1'>%s</seg>\n" % " ".join(words))
           
				out.write('</doc>\n')
        out.write('</%sset>\n' % flag)
        out.write("</mteval>\n")

def readScores(path):
	with open(path, 'r') as f:
		scores = []
		for line in f.readlines():
			match = re.search(r'4-grams = (0\.[0-9]+)',line)
			if match:
				scores.append(float(match.group(1)))
	return scores

def run_Ttest():
    f_sch = numpy.array(readScores('../data/tests/BLUE_score/Fable-Sch.txt'))
    f_per = numpy.array(readScores('../data/tests/BLUE_score/Fable-Personage.txt'))
    
    print "Mean scheherazade:", numpy.mean(f_sch)
    print "Mean realpro:", numpy.mean(f_per)
    print "Var scheherazade:", numpy.std(f_sch)
    print "Var realpro:", numpy.std(f_per)


    # independent t-test
    t1_statistic, p1_value = stats.ttest_1samp(f_sch - f_per, 0)
 
    # p < 0.05 => alternative hypothesis:
    # the difference in mean is not equal to 0
    print "independent t-test",t1_statistic, p1_value 

if ( __name__ == '__main__'):
	if len(sys.argv) < 3 or sys.argv[2] not in FLAGS:
		sys.exit('Usage: python buildXml4BLUE.py path_to_dataset [ref|tst]')

	createXML(sys.argv[1], sys.argv[2])