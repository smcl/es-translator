/**
 * defines verbs as forTo, withoutTo, or NoInf
 */
public enum InfType {
	ForTo,
	WithoutTo,
	NoInf
}
