import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.elements.AssignedModifier;
import story.scheherazade.misc.NounName;
import story.scheherazade.verbalizer.Verbalizer;
import story.scheherazade.verbalizer.VerbalizerState;
import story.scheherazade.verbalizer.ModifierGrammaticalType;
import story.scheherazade.elements.ValidAction;

/**
 * Lexico-syntactic class for 'extra' node, can be anything we define
 */
public class SchAttr implements SchLexSynt{
    /** */
    String nodeClass, lexeme, relation;

    /** */
	ArrayList<SchLexSynt> children;
    SchLexSynt parent;

    /**
     *
     * @param c
     * @param l
     * @throws Scheherexception
     */
	public SchAttr(String c, String l) throws Scheherexception {
		nodeClass = c;
		lexeme = l;
        relation = "";
		children = new ArrayList<SchLexSynt>();
	}

    public SchAttr(String c, String l, String r) throws Scheherexception {
        nodeClass = c;
        lexeme = l;
        relation = r;
        children = new ArrayList<SchLexSynt>();
    }

    /**
     *
     * @return
     */
	public Map<String,String> getFeatures(){
		HashMap<String, String> features = new HashMap<String, String>();
        if (lexeme == "IF") {
            features.put("starting_point", "+");
        }
		else {
            features.put("class", nodeClass);
        }
		features.put("lexeme", lexeme);
        if (!relation.equals("")) {features.put("rel", relation);}
		return features;
	}

    /**
     * Returns the elements this node governs
     * @return
     */
	public ArrayList<SchLexSynt> getChildren(){
		return children;
	}

    /**
     * Sets the elements this node governs
     * @param child
     */
	public void setChild(SchLexSynt child){
		this.children.add(child);
	}

    public void setParent(SchLexSynt p) {parent = p;}
    public SchLexSynt getParent() {return parent;}

    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

    public void setRelation(String rel) {this.relation = rel;}

    /**
     * Deep SchAttr copy
     * @return
     */
    public SchAttr createCopy() {
        SchAttr newAttr = null;
        try {
            newAttr = new SchAttr(this.nodeClass, this.lexeme, this.relation);
            newAttr.setParent(this);
            for (SchLexSynt child : this.children) {
                //newAttr.children.add(child.createCopy());
                newAttr.setChild(child.createCopy());
            }
        } catch (Scheherexception scheherexception) {
            scheherexception.printStackTrace();
        }
        return newAttr;
    }
}
