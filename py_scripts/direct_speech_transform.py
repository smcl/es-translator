#!/usr/bin/python
# Stephanie Lukin, Lena Rishes

import sys
import nltk
from random import randint
from nltk.corpus import stopwords

# Transforms a direct speech utterance realized by RealPro into different formats of direct speech
class transformDS:
    def __init__(self, filename, format):
        """
		@param fn: train, test or dev folder
		"""
        self.filename = filename
        self.format = format

    def extract_from_realpro(self):
        with open(self.filename, 'r') as realpro_file:
            story = realpro_file.read()
            return story

    def extract_from_personage(self):
        #TODO: FIX PATH
        with open(self.filename, 'r') as realpro_file:
            story = realpro_file.read()
            return story


    def insert_quotes(self, story):
        new_story = ""
        sentences = nltk.sent_tokenize(story)

        for sentence in sentences:
            if "the DIRECT*SPEECH" in sentence or "The DIRECT*SPEECH" in sentence:

                #The crow thought the DIRECT*SPEECH I will eat the cheese on the tree's branch.
                # presuppose the subject communication_verb the DIRECT*SPEECH
                words = nltk.word_tokenize(sentence)

                # E.g. The Crow sang
                if words[0].lower() in stopwords.words('english'):
                    subject = words[0] +" "+ words[1]
                    verb = words[2]

                # e.g. Danny sang
                else:
                    subject = words[0]
                    verb = words[1]

                speech_act = sentence[sentence.index("DIRECT*SPEECH") + len("DIRECT*SPEECH")+1: len(sentence)]


                style = randint(1,10)
                if style % 2 == 0:
                    new_sentence = subject + " " + verb + " \"" + speech_act + "\""

                else:
                    upper_speech_act = ""
                    first_word = True
                    for word in nltk.word_tokenize(speech_act):
                        if first_word:
                            title_case_word = word[0].upper() + word[1:]
                            upper_speech_act += (title_case_word) + " "
                            first_word = False
                        else: upper_speech_act += (word) + " "
                    new_sentence = "\"" + upper_speech_act[0:len(upper_speech_act)-3] + "\", " + subject.lower() + " " + verb +"."
                    # this space is added when tokenizing
                    new_sentence = new_sentence.replace(" \'", "\'")


                # TODO: if the previous sentence speak is the same as this sentence,
                # we can continue without putting the speaker. Or just
                # append the two together
                #elif self.format == "4":
                #    print "\"Blah blah\"."
                #    print "\"" + utterance + "\"."
                new_story += new_sentence +" "

            else:
                new_story += sentence +" "




        file = open(self.filename, 'w')
        file.write(new_story)
        file.close()



if ( __name__ == '__main__'):
    # example: direct_speech_transform firepath/fox-crow-directspeech
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: %s test_set_name filename [realpro/pers]' %
            sys.argv[0])

    else:
        e = transformDS(sys.argv[1], sys.argv[2])
        if sys.argv[2] == "realpro":
            story = e.extract_from_realpro()
        else:
            story = e.extract_from_personage()

        e.insert_quotes(story)