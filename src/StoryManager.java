
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.*;
import edu.mit.jwi.Dictionary;

import story.scheherazade.elements.*;
import story.dbcore.links.*;
import story.scheherazade.misc.*;
import story.scheherazade.parameters.*;
import story.scheherazade.admin.*;
import story.scheherazade.prologuer.COMLEXVerbTense;
import story.scheherazade.prologuer.Comlex;
import story.scheherazade.prologuer.Preposition;
import story.scheherazade.verbalizer.*;
import story.scheherazade.exceptions.*;
import story.dbcore.exceptions.*;
import story.scheherazade.virgil.*;
import java.io.*;

/**
 * Scheherazade-to-Personage intermediary.
 * iterates through .vgl file and extracts story information to create SchLexSynt nodes
 *
 * Stephanie M.Lukin and Elena Rishes
 * 05/2012
 *
 * Scheherazade API courtesy of David K. Elson
 */
public class StoryManager {

    public static Comlex comlex;
    public static StoryInterpreter storyInterpreter;
    private static BuildDSyntS dsynts;
    private static TreeMap<Integer, String> sentence_relations;
    private static Verbalizer verbalizer;
    private static VerbalizerState verbalizerState;
    private static StoryListener storyListener;
    private Style vglStyle;
    public static String original_story;
    public static Parameters parameters;

    public String vgl_file, xml_file, sch_file, textplan_file, assertionFile;

    public VirgilFile virgilFile;

    public Assertions assertions;

    private static String name;

    /**
     * path to Personage domain folder
     */
    private String domain = null;

    /**
     * flag that signals that textplans are needed in addition to dsynts
     */
    private static boolean buildTextplan = false;

    /**
     * flag that controls what's output to console
     */
    private static boolean displayDebug = true;

    // list of all characters who could potentially be narrators
    public static ArrayList<SchNoun> storyCharacters;

    // a list of the pairs of events and the links between them, to be dumped to a file
    public ArrayList<ArrayList> storyGraph;

    public static ArrayList<ArrayList<Object>> allActions;

    public static IDictionary dict;


    /**
     * Class constructor to verbalize story with RealPro
     *
     * @param storyListener
     * @param verbalizer
     * @param name
     * @param original      String containing the original story; may be ""
     * @throws Scheherexception
     */
    public StoryManager(StoryListener storyListener, Verbalizer verbalizer, String name, String original) throws Scheherexception, IOException {

        StoryManager.storyListener = storyListener;
        storyInterpreter = new StoryInterpreter(storyListener);
        StoryManager.verbalizer = verbalizer;
        dsynts = new BuildDSyntS();
        StoryManager.name = name;
        storyCharacters = new ArrayList<SchNoun>();
        storyGraph = new ArrayList<ArrayList>();
        original_story = original;
        sentence_relations = new TreeMap<Integer, String>();
        allActions = new ArrayList<ArrayList<Object>>();
        initJWI();
    }

    /**
     * Class constructor to translate story into Personage
     *
     * @param storyListener
     * @param verbalizer
     * @param name
     * @param original      String containing the original story; may be ""
     * @param path
     * @throws Scheherexception
     */
    public StoryManager(StoryListener storyListener, Verbalizer verbalizer,
                        String name, String original, String path) throws Scheherexception, IOException {
        this(storyListener, verbalizer, name, original);
        domain = path;
        buildTextplan = true;
        storyCharacters = new ArrayList<SchNoun>();
        storyGraph = new ArrayList<ArrayList>();
        original_story = original;
        sentence_relations = new TreeMap<Integer, String>();
        allActions = new ArrayList<ArrayList<Object>>();
        initJWI();
    }

    public void setParameters(Parameters params) {
        parameters = params;
    }

    public void setAssertions(String assertionFile, String prologFile) {
        assertions = new Assertions(assertionFile, prologFile);
    }

    public void setStory(String s) {
        original_story = s;
    }

    public void setFiles(String vgl_f, String xml_f, String sch_f, String text_f) {
        vgl_file = vgl_f;
        xml_file = xml_f;
        sch_file = sch_f;
        textplan_file = text_f;
    }

    public static void initJWI() throws IOException {
        try {
            String wnhome = "jwnl/WordNet-3.0";
            String path = wnhome + File.separator + "dict";
            URL url = new URL("file", null, path);
            // construct the dictionary object and open it
            dict = new edu.mit.jwi.Dictionary(url);
            //dict.open();
        } catch(IOException ee) {
            System.err.print(ee);}
    }

    // given a word offset, assume it's a verb, we find the derivationally related words
    // of those, we find the nouns that end in ing
    public String getGerund(long offset) {
        if (offset == -1) {return "";}

        ISynset word = dict.getSynset(new SynsetID((int)offset, POS.VERB));
        if (word == null){return "";}
        for (IWordID relatedWordID: word.getWords().get(0).getRelatedWords(Pointer.DERIVATIONALLY_RELATED)) {
            IWord relatedWord = dict.getWord(relatedWordID);
            if (relatedWord.getPOS().equals(POS.NOUN) && relatedWord.getLemma().endsWith("ing")) {
                System.out.println("Word: " + word);
                System.out.println("Gerund: " + relatedWord.getLemma());
                return relatedWord.getLemma();
            }
        }
        return "";
    }


    /*******************************************************************************************************************/
    /*  Getting started
    /*******************************************************************************************************************/

    /**
     * Beginning of the transformation pipeline
     *  We start by reading in the vgl file and iterate through annotated story points
     *  Print the Scheherazade annotation for each story point
     *  Convert story point to SchLexSynt and add it to the dsynts list for this story
     *  If we want, we can extract the event pairs
     *  Finally, we write the Scheherazade realization to a file
     * @throws Scheherexception
     * @throws IOException
     */
    public void processVGL() throws Scheherexception, IOException, Exception {
        loadStoryPoints(vgl_file); // load the story point from the given vgl file
        System.out.println("Finished loading story points from vgl");

        String directory = new File("").getAbsolutePath();
        if (directory.contains("es-translator\\src")) {
            // then it's a window's machine
            directory = directory.substring(0, directory.indexOf("es-translator\\src")) + "es-translator";
        }
        String comlex_path = directory + "/comlex/comlex_synt_1.1.1";
        comlex = new Comlex(comlex_path);

        // iterate through conditions/properties
        AssignedConditionSet conditionSet = storyInterpreter.getAllConditions(TimelineID.REALITY);

        // iterate through all story points
        System.out.println("Iterate through story points");
        AssignedActionSet actionSet = storyInterpreter.getAllActions(TimelineID.REALITY);
        for (Iterator<AssignedAction> i = actionSet.iterator(); i.hasNext();) {
            // print each point
            AssignedPredicate action = i.next();
            if (!isInterp(action)) {
                displayAction(action);

                // convert point to xml
                SchVerb verb = returnAction(action, true);

                // get source text if applicable
                if (action.getSourceSpan() != null) {
                    String sourceText = original_story.substring(action.getSourceSpan().begin(), action.getSourceSpan().end());
                    verb.setSourceText(sourceText);

                    // find assertions that match this source text
                    ArrayList<String> emotions = assertions.find(sourceText);
                    verb.setEmotions(emotions);
                }

                System.out.println();

                ArrayList<SchNoun> sentenceNouns = new ArrayList<SchNoun>();
                getSentenceNouns(verb, sentenceNouns);
                appendProperties(verb, conditionSet, sentenceNouns);


                //**************************************//

                // gerund
                /**
                for (SchLexSynt child: verb.getChildren()) {
                    if (child instanceof SchVerb && ((SchVerb)child).assignedAction && ((verb.sameObjSubj((SchVerb) child) && ((SchVerb) child).hasObj()) || (!((SchVerb) child).hasObj()))) {
                        ((SchVerb)child).removeSubj();
                        ((SchVerb)child).setMood("gerund");
                        ((SchVerb)child).setLexeme(getGerund((((SchVerb)child).getOffset())));
                    }
                }
                **/

                // SPEECH ACTS
                // speaker_type is the noun, e.x. wildcat, person
                // speaker_name may be defined, e.x. Jenny, Bob
                // if there is no supplied speaker name, speaker_name will default to the empty string
                // and we need to pass speaker_type to BuildDsynts class to fill 'voice' attribute of textplan
                // otherwise, can pass speaker_name as the 'voice
                /**
                String speaker_type = "";
                String speaker_name = "";

                // TODO: MAKE THIS SOME KIND OF PARAMETER
                // direct, indirect, free, narrator
                String param = "direct";

                // handle grammar changes (person and tense) here
                // TODO: talked to
                String[] blacklist = {"ask", "talk", "require", "sign", "order", "agree", "catalogue", "decide", "desire"}; //don't want speech for these
                if (verb.isPredicate() && verb.isVerbOfSpeech() & !(Arrays.asList(blacklist).contains(verb.getLexeme()))) {
                    System.err.println("FOUND DIRECT SPEECH INSTANCE");

                    try {
                        // first extract the speaker by iterating over the children of the verb.
                        // we are looking for a SchNoun child with rel=I (i.e. index=0)
                        for (SchLexSynt child : verb.getChildren()) {
                            if (child instanceof SchNoun && ((SchNoun) child).index == 0) {
                                speaker_type = ((SchNoun) child).getLexeme();
                                speaker_name = ((SchNoun) child).name;
                                break;  // don't need to keep looking after we found the speaker
                            }
                        }

                        // if we are communicating a verb frame
                        for (SchLexSynt child : verb.getChildren()) {
                            if (child instanceof SchVerb) {
                                speechVerb(verb, ((SchVerb) child), speaker_name, speaker_type, param);
                            }
                        }
                    } catch (ClassCastException s) {
                        throw new Scheherexception("Unexpected direct speech structure in " + name);
                    }
                }
                 **/

                // "The narrator planned for her to make the mead"
                // This doesn't handle speech: "The crow thought to eat"
                // this is dealing with the second verb. We need to remove the "for her" by removing the subject
                // RULES FOR SECOND VERB IN SENTENCE
                // The narrator's father and the narrator (I) told the phone company (II) the telephone was broken (III).
                for (SchLexSynt child: verb.getChildren()) {
                    if (child instanceof SchVerb) {
                        ((SchVerb)child).index = 2;
                    }
                }

                // checks if we have a verb verb case
                checkForInfinitives(verb);

                // TODO: if then
                // restructure tree for if then conditionality
                verb = findIfThenClause(verb);

                // check for nested coordintaion of verbs
                coordVerbs(verb, false, true);

                // TODO: gerunds
                //long offset = 1939174;
                //String h=getGerund(offset);

                // restructure tree for "is able to"
                checkIsAble(verb);

                // add the newly created SchLexSynt verb to the dsynts list
                dsynts.addDSynt(verb);
            }
        }

        //TODO: testing interp integration; keep commented out if not developing
        // 1) find timeline links to interp elements
        // 2) determine how to realize interp elements
        // 3) insert interp elements in the appropriate place in the story
        // (I see an instance where different timeline nodes point to the same interp; in this case can't just have interp node realized before timeline)
        // (for this example, I guess we can only realize an interp element once, and it will be before the first timeline node it is linked to)
        //for (ArrayList<Object> storyPointSequence : allActions) {
        //    getInterpSequence(storyPointSequence);
        //}

        // print the realization from Scheherazade to a file
        if (sch_file != null && !sch_file.isEmpty()){
            printScheherazadeRealization(sch_file);
            System.out.println("Finished writing scheherazade realization");
        }
    }

    // func/verb
    //  verb
    //  and
    //   verb rel=II

    // verb
    //  and
    //   verb rel=III
    public void coordVerbs(SchLexSynt node, boolean clause, boolean inf) {
        // if two verbs in a row
        if (node.getChildren().size() >= 2) {
            SchLexSynt prev ;
            SchLexSynt curr = node.getChildren().get(0);
            for (int i = 1; i < node.getChildren().size(); i++) {
                prev = curr;
                curr = node.getChildren().get(i);

                // if the prev clause has an 'in order to'
                for (SchLexSynt child: prev.getChildren()) {
                    if (child instanceof SchFunc || child instanceof SchAttr) {
                        clause = true;}
                }
                for (SchLexSynt child: curr.getChildren()) {
                    if (child instanceof SchFunc) {
                        clause = true;}
                }


                if (curr instanceof SchVerb && prev instanceof SchVerb) {
                    ((SchVerb)curr).index = 1;
                    if (clause) {
                        ((SchVerb)curr).index = 1;
                    }
                    if (((SchVerb)curr).getToInf() == InfType.ForTo) {
                        ((SchVerb)curr).index = 1;
                        ((SchVerb)curr).setToInf(InfType.NoInf);
                    }
                    // create coord clause
                    ArrayList<SchLexSynt> coord = new ArrayList<SchLexSynt>();
                    // set second verb as child of coord and remove from res
                    coord.add(curr);
                    node.getChildren().remove(curr);
                    // set coord as child of res
                    SchFunc and = new SchFunc("and", coord);
                    node.getChildren().add(and);
                    break;
                }
            }
        }

        if (node instanceof SchFunc) {clause = true;}
        else {clause = false;}
        if (node instanceof SchVerb) {inf = true;}
        else {inf = false;}
        for (SchLexSynt child: node.getChildren()) {
            coordVerbs(child, clause, inf);
        }
    }

    public void checkIsAble(SchLexSynt node) {
        // base case verbs
        if (node instanceof SchVerb && ((SchVerb)node).getLexeme().equals("able")) {
            ((SchVerb)node).setLexeme("be");
            try {
                ((SchVerb)node).setChild(new SchAdj("able"));
            } catch (Scheherexception e) {
                System.out.println(e);
            }
            SchFunc to = new SchFunc("TO");
            // append any additional children
            for (SchLexSynt child : ((SchVerb)node).getChildren()) {
                if (child instanceof SchVerb) {
                    ((SchVerb) child).setToInf(InfType.WithoutTo);
                    ((SchVerb) child).mood = "inf"; // don't need to 'to'
                    ((SchVerb) child).removeSubj();
                    to.setChild(child);
                    ((SchVerb)node).removeChild(child);
                }
            }
            ((SchVerb)node).setChild(to);}

        else {
            for (SchLexSynt child: node.getChildren()) {
                checkIsAble(child);
            }
        }
    }


    /**
     * Generates a dsynts from a 3rd person narrative perspective
     * @throws IOException
     */
    public void toRealPro() throws IOException, Scheherexception {
        dsynts.initTextPlans(); // set the size for the text plan array, used in the next method
        // save a copy of the original, unsplit dsynts
        dsynts.copyDsynts();
        // modify this object to conatin as simple sentences as possible and create text plans inside this method
        dsynts.splitDSynts(storyInterpreter, parameters, TimelineID.REALITY, verbalizerState);

        //dsynts.saveDSynt(xml_file); // output the xml struture to this path
        //dsynts.buildTextPlans(textplan_file);
        //System.out.println("Finished writing dsynts");
    }

    public void saveOutput() throws IOException, Scheherexception {
        System.out.println("Writing dsynts...");
        dsynts.saveDSynt(xml_file); // output the xml struture to this path
        dsynts.buildTextPlans(textplan_file);
        System.out.println("Finished writing dsynts");
    }

    /**
     * Generates a dsynts from a 1st person narrative perspective given this narrator
     * @throws Scheherexception
     * @throws IOException
     */
    public void toRealPro(String xml_file, String textplan_file, String characterName, SchNoun narrator) throws IOException, Scheherexception {
        dsynts.initTextPlans(); // set the size for the text plan array, used in the next method
        dsynts.resetDsynts(); // revert to unaltered dsynts

        // save a copy of the original, unsplit dsynts
        dsynts.copyDsynts();
        // modify this object to conatin as simple sentences as possible and create text plans inside this method
        dsynts.splitDSynts(storyInterpreter, parameters, TimelineID.REALITY, verbalizerState);

        dsynts = changeNarrator(dsynts, narrator);

        // need to reset certain features of SchNoun that are modified when we change narrator
        for (SchLexSynt root_verb: dsynts.getDsynts()) {
            dsynts.changePossession(root_verb, characterName, ((SchVerb)root_verb).isSpeech);
        }

        //dsynts.saveDSynt(xml_file); // output the xml struture to this path
        //dsynts.buildTextPlans(textplan_file);
        //System.out.println("Finished writing dsynts");
    }

    void copyDsynts() { dsynts.copyDsynts();}

    /**
     * Generates a personage text plan and outputs to the Personage domain folder
     * from a 1st person narrative perspective given this narrator
     * @throws IOException
     */
    public void toPersonage(String characterName, SchNoun narrator, boolean deagg) throws IOException, Scheherexception {
        // NOTE: these only have an impact if we are doing personage, NOT realpro
        dsynts.initTextPlans(); // set the size for the text plan array, used in the next method
        dsynts.resetDsynts(); // revert to unaltered dsynts

        if (deagg) {
            // save a copy of the original, unsplit dsynts
            dsynts.copyDsynts();
            // modify this object to conatin as simple sentences as possible and create text plans inside this method
            dsynts.splitDSynts(storyInterpreter, parameters, TimelineID.REALITY, verbalizerState);
        }
        else {
            dsynts.defaultTextPlans();
        }

        dsynts = changeNarrator(dsynts, narrator);
        // need to reset certain features of SchNoun that are modified when we change narrator
        for (SchLexSynt root_verb: dsynts.getDsynts()) {
            dsynts.changePossession(root_verb, characterName, ((SchVerb)root_verb).isSpeech);
        }

        // output the xml struture to this path
        dsynts.saveDSynt2Domain(domain, name+parameters.toString());
        //if (deagg) {dsynts.saveDSynt2Domain(domain, name+"-deagg_"+characterName+parameters.toString());}
        //else {dsynts.saveDSynt2Domain(domain, name+"_"+characterName+parameters.toString()); }
        System.out.println("Finished writing dsynts");
    }

    /**
     * Generates a personage text plan and outputs to the Personage domain folder
     * @throws IOException
     */
    public void toPersonage(boolean deagg) throws IOException, Scheherexception {
        // NOTE: these only have an impact if we are doing personage, NOT realpro
        dsynts.initTextPlans(); // set the size for the text plan array, used in the next method

        if (deagg) { // deagg includes speech, in order tos, synchonous
            // save a copy of the original, unsplit dsynts
            dsynts.copyDsynts();
            // modify this object to conatin as simple sentences as possible and create text plans inside this method
            dsynts.splitDSynts(storyInterpreter, parameters, TimelineID.REALITY, verbalizerState);
        }
        else {
            dsynts.defaultTextPlans();
        }

        // output the xml struture to this path
        // output the xml struture to this pathde
        dsynts.saveDSynt2Domain(domain, name+parameters.toString());
        //if (deagg) {dsynts.saveDSynt2Domain(domain, name+"-deagg"+parameters.toString());}
        //else {dsynts.saveDSynt2Domain(domain, name+parameters.toString());}
        System.out.println("Finished writing dsynts");
    }

    /*******************************************************************************************************************/
    /* Process vgl
    /*******************************************************************************************************************/

    /**
     * Loads story points from the provided VGL file
     * @param vgl_file vgl file path
     * @throws Scheherexception
     */
    public void loadStoryPoints(String vgl_file) throws Scheherexception {
        vglStyle = new Style();
        vglStyle.set(StyleParam.AnonymousDisambiguators, Style.ANONYMOUS_DISAMBIGUATORS_OFF);
        vglStyle.set(StyleParam.AllNounsGiven, Style.ALL_NOUNS_GIVEN_OFF);
        vglStyle.set(StyleParam.RealityPerception, Style.REALITY_PERCEPTION_POSITIVE);
        vglStyle.set(StyleParam.NounPropertiesInIntroductions, Style.NOUN_PROPERTIES_IN_INTRODUCTIONS_ON);
        vglStyle.set(StyleParam.Modifiers, Style.MODIFIERS_ON);
        vglStyle.set(StyleParam.Debug, Style.DEBUG_OFF);
        vglStyle.set(StyleParam.AllowNames, Style.ALLOW_NAMES_OFF);
        System.out.println("Done.  Loading files...");

        final VirgilFile file = new VirgilFile(new File(vgl_file));
        System.out.println("Loaded all files, parsing stories...");

        // If VirgilFile encountered an error, quit
        if (file.errorDuringParse()) {
            file.getParseError().printStackTrace();
            System.exit(1);
        }

        storyInterpreter.initializeNarrative(new NarrativeID(name));
        StoryTelling telling = file.getStoryTelling();
        for (int j=0; j < telling.size(); j++) {
            StoryPoint point = telling.get(j);
            //System.err.println("#### POINT: "+point.render());

            try {
                storyListener.hear(point);
            } catch (Scheherexception s) {
                throw new Scheherexception("Error thrown when parsing point \""+point.render()+"\": "+s.getMessage());
            }
        }
        System.err.println("#### DONE LOADING POINTS");
    }

    /**
     * Display an AssignedAction in both raw propositional notation
     * and serialized natural language.
     * @throws Scheherexception
     */
    public static void displayAction(AssignedPredicate action) throws Scheherexception {
        verbalizerState = verbalizer.freshState();
        verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

        if (displayDebug) {
            try {
                System.out.println("   -- " + action.render());
                TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));
                VerbalizeAssignedAction vaa = new VerbalizeAssignedAction(((AssignedAction) action), ((AssignedAction) action).getTime(), perceptionTime, VerbTense.PastSimple, false);
                String actionVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, vaa, verbalizerState).toString();
                System.out.println("       ** \"" + actionVerbalization + "\"");
            } catch (Scheherexception sch) {
                System.out.println("No verbalization possible for " + ((AssignedAction) action).getAction().getType().toString() + ".");
            }

            // prints out the segment from the original story that was annotated with this AssignedAction and any of its modifiers
            //if (original_story != "") {
            //    System.out.println(original_story.substring(action.getSourceSpan().begin(), action.getSourceSpan().end()));
            //    for (Iterator<AssignedModifier> j = storyInterpreter.getModifiersOfPredicate(action).iterator(); j.hasNext(); ) {
            //        AssignedModifier mod = j.next();
            //        System.out.println(original_story.substring(mod.getSourceSpan().begin(), mod.getSourceSpan().end()));
            //    }
            //}
        }
    }

    public static void displayAction(AssignedAction action) throws Scheherexception {
        verbalizerState = verbalizer.freshState();
        verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

        if (displayDebug){
            try{
                System.out.println("   -- "+action.render());

                TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));

                String actionVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(action, action.getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
                System.out.println("       ** \""+actionVerbalization+"\"");
            }
            catch (Scheherexception sch) {
                System.out.println("No verbalization possible for "+action.getAction().getType().toString()+".");
            }

            // prints out the segment from the original story that was annotated with this AssignedAction and any of its modifiers
            if (original_story != "") {
                System.out.println(original_story.substring(action.getSourceSpan().begin(), action.getSourceSpan().end()));
                for (Iterator<AssignedModifier> j = storyInterpreter.getModifiersOfPredicate(action).iterator(); j.hasNext(); ) {
                    AssignedModifier mod = j.next();
                    System.out.println(original_story.substring(mod.getSourceSpan().begin(), mod.getSourceSpan().end()));
                }
            }
        }
    }

    public static boolean isInterp(AssignedPredicate action) throws Scheherexception {
        return (((AssignedAction) action).getTime().toString().contains("Interp"));
    }


    /**
     * Converts an action into a SchLexSynt Verb. getSCHArguments may return nested propositions
     * 0) Condition ends (hack)
     * 1) Property
     * 2) Valid  Condition
     * 3) Assigned Condition
     * 4) Valid Action
     *
     * @param
     * @throws Scheherexception
     */
    public static SchVerb returnAction(AssignedPredicate action, boolean bMakePredicate) throws Scheherexception {
        SchVerb verb = null;
        allActions.add(getInterpSequence(action, new ArrayList<Object>()));

        // 0) Condition ends: the actual action is the argument of this action; find the nested action
        if (action instanceof AssignedAction && (((AssignedAction) action).getAction()).getPrototypeNode().toString().contains("conditionEnds")) {
            Object argument = null;
            for (Enumeration<Object> j = ((AssignedAction) action).getAction().getSCHArguments().elements(); j.hasMoreElements(); )
                argument = j.nextElement();

            if (argument instanceof AssignedCondition) {
                verb = new SchVerb((AssignedPredicate)argument, 1, storyInterpreter, TimelineID.REALITY, verbalizerState);
            }
            verb.setConditionality((action).getConditionality());
            verb.truthDegree = -1; // 'condition Ends' is a negation, so we change truth degree
        }

        // 1) Property (ValidCondition)
        // this is a hack to get rid of "begins/ends" for Properties defined as a separate action
        else if (action instanceof AssignedAction &&
                (((AssignedAction) action).getAction().getType().toString().contains("conditionBegins")
                        || ((AssignedAction) action).getAction().getType().toString().contains("conditionEnds"))
                && ((AssignedAction) action).getAction().getSCHArguments().get(0) instanceof ValidCondition) {

            verb = new SchVerb(((AssignedAction) action), 1, storyInterpreter, TimelineID.REALITY, verbalizerState);
            //verb = new SchVerb((ValidCondition)((AssignedAction) action).getAction().getSCHArguments().get(0), bMakePredicate, storyInterpreter, TimelineID.REALITY, verbalizerState);
            verb.setConditionality((action).getConditionality());
        }

        // 3) Assigned Condition
        else if (action instanceof AssignedCondition) {
            verb = new SchVerb((AssignedPredicate)action, 1, storyInterpreter, TimelineID.REALITY, verbalizerState);
        }

        // 4) Assigned Action
        else {
            verb = new SchVerb((AssignedPredicate)action, 1, storyInterpreter, TimelineID.REALITY, verbalizerState);
            verb.setConditionality((action).getConditionality());
        }


        // TODO: temporary conditional mood logic here
        /**
        for (SchLexSynt child : verb.getChildren()) {
            if (child instanceof SchVerb && ((SchVerb) child).conditionality != null && ((SchVerb) child).conditionality == Conditionality.If)
                for (SchLexSynt otherChild : verb.getChildren()) {
                    if (otherChild instanceof SchVerb && ((SchVerb) otherChild).conditionality != null && ((SchVerb) otherChild).conditionality != Conditionality.If)
                        ((SchVerb) otherChild).setMood("cond");
                    otherChild.getChildren().add(new SchAttr("ATTR", "then"));
                }
        }
         **/

        // find potential narrators that are children of this verb
        addNarrators(verb);

        //TODO: comment out for now, we might need a better method for doing this
        // resolve any coreferences in the sentence (start with empty list)
        //findCoreferences(verb, new ArrayList<SchNoun>());

        // insert verb and verb coordination between siblings
        // call a method in SchVerb that takes the Schverb and looks for SchVerb siblings)
        // verb.addCoord(dsynts);
        // TODO: if siblings are SchVerbs, make them a new textplan with the synch relationship (?)
        return verb;
    }

    public void appendProperties(SchVerb verb, AssignedConditionSet conditionSet, ArrayList<SchNoun> sentenceNouns) throws Scheherexception {
        for (Iterator<AssignedCondition> i = conditionSet.iterator(); i.hasNext();) {
            AssignedCondition condition = i.next();
            // skip interpretation layer
            if (condition.getInterpKey() != null) {continue;}
            ValidPredicate p = condition.getValidPredicate();
            NounName nounName = (NounName) p.getSCHArguments().get(0);

            for (SchNoun noun: sentenceNouns) {
                if (noun.nounNameID.equals(nounName.toString())) {
                //if (noun.nounNameID.contains(nounName.getType().toString().substring(2, nounName.getType().toString().length() - 2))) {
                    ValidCondition vc = condition.getCondition();
                    if (vc.getType().toString().contains("ownSomething")
                            || vc.getType().toString().contains("ableToAct")
                            || vc.getType().toString().contains("beliefConditionPresent")
                            || vc.getType().toString().contains("on/around/over/etc. something")
                            ) {
                        continue;}
                    else if (vc.getType().toString().contains("moreThanCondition")) {
                        NounName arg0 = (NounName) vc.getSCHArguments().get(0);
                        NounName arg1 = (NounName) vc.getSCHArguments().get(1);
                        ValidNoun arg0vn = storyInterpreter.findNounDefinition(TimelineID.REALITY, arg0);
                        ValidNoun arg1vn = storyInterpreter.findNounDefinition(TimelineID.REALITY, arg1);
                        ValidCondition c = (ValidCondition) vc.getSCHArguments().get(2);

                        // arg0 is more c than arg1
                        SchNoun arg0n = new SchNoun(arg0vn, "", 0, storyInterpreter, TimelineID.REALITY, verbalizerState);
                        SchVerb is = new SchVerb("is");
                        SchAttr more = new SchAttr("", "more");
                        SchAdj cond = new SchAdj(c.getType().toString());
                        SchAttr than = new SchAttr("", "than");
                        SchNoun arg1n = new SchNoun(arg1vn, "", 0, storyInterpreter, TimelineID.REALITY, verbalizerState);

                        than.setChild(arg1n);
                        cond.setChild(than);
                        more.setChild(cond);
                        is.setChild(more);
                        arg0n.setChild(is);
                        //noun.getParent().setChild(arg0n);

                        System.out.println(arg0.toString() + " is more " + c.getType().toString() + " than " + arg1);
                    }
                    // character that is <adj>
                    else if (vc.getType().toString().contains("propIsProp") ||
                            vc.getType().toString().contains("charIsChar")) {
                        if (!(vc.getSCHArguments().get(0) instanceof NounName && vc.getSCHArguments().get(1) instanceof NounName)) {continue;}
                        NounName arg0 = (NounName) vc.getSCHArguments().get(0);
                        NounName arg1 = (NounName) vc.getSCHArguments().get(1);

                        // ignore if gender
                        if (new SchNoun(storyInterpreter.findNounDefinition(TimelineID.REALITY, arg0),
                                "", 1, storyInterpreter, TimelineID.REALITY, verbalizerState).getLexeme().equals("female")) {continue;}
                        if (new SchNoun(storyInterpreter.findNounDefinition(TimelineID.REALITY, arg0),
                                "", 1, storyInterpreter, TimelineID.REALITY, verbalizerState).getLexeme().equals("male")) {continue;}

                        String truthDegree = "is";
                        if (vc.getTruthDegree() < 0) {truthDegree = "is not";}
                        System.out.println(arg0.toString() + " that " + truthDegree + " " + arg1.toString());

                        // create a clause "that is a (arg1)
                        SchFunc that = new SchFunc("that");
                        SchVerb v = new SchVerb("is");
                        v.setTruthDegree(vc.getTruthDegree());
                        ValidNoun n = storyInterpreter.findNounDefinition(TimelineID.REALITY, arg1);
                        SchNoun arg1Noun = new SchNoun(n, "", 1, storyInterpreter, TimelineID.REALITY, verbalizerState);
                        v.setChild(arg1Noun);
                        that.setChild(v);
                        //noun.getParent().setChild(that);
                    }
                    else {
                        System.out.println(vc.getType().toString() + " modifies " + noun.nounNameID);
                        SchAdj adj = new SchAdj(vc.getType().toString());
                        adj.setPremodifier();
                        noun.setProperty(adj.getLexeme());

                        // if the noun is a possession
                        // e.g. the crazy squirrel's paw
                        for (SchLexSynt child : noun.getChildren()) {
                            if (child instanceof SchNoun) {
                                // make sure it's not a nested nested
                                // e.g. the narrator's brother's flashlight
                                for (SchLexSynt grandchild : child.getChildren()) {
                                    if (grandchild instanceof SchNoun) {
                                        if (!grandchild.getChildren().contains(adj)) {
                                            // only append property to SchNoun if it's not first person
                                            // if this noun is the POV from which we're telling, skip appending properties
                                            if (parameters.getNarr_pov() == 1 && !parameters.getNarr_foc().equals(((SchNoun) grandchild).getLexeme())) {
                                                grandchild.setChild(adj);
                                            }
                                            ((SchNoun)grandchild).setProperty(adj.getLexeme());
                                            return;
                                        }
                                    }
                                }
                                if (!child.getChildren().contains(adj)) {
                                    // only append property to SchNoun if it's not first person
                                    // if this noun is the POV from which we're telling, skip appending properties
                                    if (parameters.getNarr_pov() == 1 && !parameters.getNarr_foc().equals(((SchNoun) child).getLexeme())) {
                                        child.setChild(adj);
                                    }
                                    ((SchNoun)child).setProperty(adj.getLexeme());
                                    return;
                                }
                            }
                        }
                        // otherwise
                        // e.g. the crazy squirrel
                        if (!noun.getChildren().contains(adj)) {
                            // only append property to SchNoun if it's not first person
                            // if this noun is the POV from which we're telling, skip appending properties
                            if (parameters.getNarr_pov() == 3 || (parameters.getNarr_pov() == 1 && !parameters.getNarr_foc().equals(noun.getLexeme()))) {
                                noun.setChild(adj);
                            }
                            noun.setProperty(adj.getLexeme());
                            return;
                        }
                    }
                }
            }
        }
    }


    public void checkForInfinitives(SchLexSynt node) {
        for (SchLexSynt child: node.getChildren()) {

            // INFINITIVE
            if (child instanceof SchVerb) { // She said she verb (not She said to verb)
                if ((node instanceof SchVerb && !((SchVerb)node).getLexeme().equals("say") && !((SchVerb)node).getLexeme().equals("tell")
                        && !((SchVerb)node).getLexeme().equals("email") && !((SchVerb)node).getLexeme().equals("think") )
                        && !((SchVerb) child).isSpeech && !(((SchVerb) child).getRelation().equals("I")) && !((SchVerb) child).getMood().equals("gerund")) {

                    // i saw    the bugs   were on the ceiling S V O V - ValidCOndition
                    if (child instanceof SchVerb && ((SchVerb) child).validCondition != null) {

                    }

                    else {
                        ((SchVerb) child).setToInf(InfType.ForTo);
                    }

                    // VERB INFINITIVE
                    // if the second subject is the same as the first and there is no object, remove the subject
                    // ex: Lillian instructed the groups for the groups to free the wasp
                    // => Lillian instructed the groups to free the wasp
                    if (node instanceof SchVerb && ((SchVerb) node).hasObj() && ((SchVerb) child).hasObj() && ((SchVerb) node).sameObjSubj((SchVerb) child)) {
                        //((SchVerb) child).setToInf(InfType.ForTo);
                        ((SchVerb) child).removeSubj();
                    }
                    // ex: The groups tried for the groups to find a container
                    // => The groups tried to find a container
                    if (node instanceof SchVerb && !((SchVerb) node).hasObj() && ((SchVerb) node).sameSubj((SchVerb) child)) {
                        //((SchVerb) child).setToInf(InfType.ForTo);
                        ((SchVerb) child).removeSubj();
                    }
                    // ex: The groups tried for the groups to find a container
                    // => The groups tried to find a container
                    if (node instanceof SchVerb && ((SchVerb) node).hasObj() && ((SchVerb) node).sameObjSubj((SchVerb) child)) {
                        //((SchVerb) child).setToInf(InfType.ForTo);
                        ((SchVerb) child).removeSubj();
                    }
                    // GERUND??
                }
            }
            // recurse
            checkForInfinitives(child);
        }
    }

    public void speechVerb(SchVerb head, SchVerb vp_speech_frame, String speaker_name, String speaker_type, String param) throws Scheherexception {
        // pass along speaker name to children
        // then if subj of verb == subj of governed verb ==> replace with 'I' and change person from 3d to first
        vp_speech_frame.setSpeech(speaker_name, speaker_type);

        // The crow thought I eat the cheese
        // Elizabeth said I do not want to live.
        // some indicator of quotation marks
        // change speaker, present tense
        if (param.equals("direct")) {
            // change speaker to I
            setSpeaker(vp_speech_frame, speaker_type, speaker_name);

            //vp_speech_frame.setTense("present");
            // TODO: must find a better way to iterate
            for (SchLexSynt child: head.getChildren()) {
                if (child instanceof SchVerb) {
                    ((SchVerb) child).setTense("present");
                }
                else if (child instanceof SchAttr || child instanceof SchFunc) {
                    for (SchLexSynt grandchild : child.getChildren()) {
                        if (grandchild instanceof SchVerb) {
                            ((SchVerb) grandchild).setTense("present");
                        }
                    }
                }
            }
        }

        // The crow thought that she would eat the cheese
        // Elizabeth said that she would not want to live.
        // insert that, conditional
        else if (param.equals("indirect")) { //verb.getLexeme().equals("mention") || verb.getLexeme().equals("tell")) {
            //vp_speech_frame.setTense("present");
            // TODO: must find a better way to iterate
            for (SchLexSynt child: head.getChildren()) {
                if (child instanceof SchVerb) {
                    ((SchVerb) child).setTense("present");
                }
                else if (child instanceof SchAttr || child instanceof SchFunc) {
                    for (SchLexSynt grandchild : child.getChildren()) {
                        if (grandchild instanceof SchVerb) {
                            ((SchVerb) grandchild).setTense("present");
                        }
                    }
                }
            }

            vp_speech_frame.setMood("cond");
            SchAttr that = new SchAttr("", "that");
            that.setRelation("III");
            vp_speech_frame.index = 1; // rel = II
            that.setChild(vp_speech_frame);
            head.setChild(that);
            head.removeChild(vp_speech_frame);
        }

        // The crow would eat the cheese
        // Elizabeth would not want to live.
        // remove verb of communication, conditional
        else if (param.equals("free")) {
            vp_speech_frame.setMood("cond");
            vp_speech_frame.index = 1; // rel = II

            //vp_speech_frame.setTense("present");
            // TODO: must find a better way to iterate
            for (SchLexSynt child: head.getChildren()) {
                if (child instanceof SchVerb) {
                    ((SchVerb) child).setTense("present");
                }
                else if (child instanceof SchAttr || child instanceof SchFunc) {
                    for (SchLexSynt grandchild : child.getChildren()) {
                        if (grandchild instanceof SchVerb) {
                            ((SchVerb) grandchild).setTense("present");
                        }
                    }
                }
            }
        }

        // The crow ate the cheese
        // Elizabeth did not want to live.
        // remove verb of communication, past
        else if (param.equals("narrator")) {
            //vp_speech_frame.index = 1; // rel = II
        }
    }

    /*
     * Purpose: Accesses the timeline of the given id name and gets all actions from it
     *   by calling returnAction from MainEST, which is the same as dispalyAction except
     *   it returns the SchLexSynt node instead of appending it to dysnts
     *
     * TODO: Makes the assumption that there is only 1 event in this timeline
     * @throws Scheherexception
     */
    public static ArrayList<SchLexSynt> accessTimeline(String tID) throws Scheherexception {
        SchLexSynt node = null;
        ArrayList<SchLexSynt> nodes = new ArrayList<SchLexSynt>();
        AssignedActionSet actionSet2 = storyInterpreter.getAllActions(TimelineID.makeTimelineID(tID));

        int rel = 1;
        for (Iterator<AssignedAction> i = actionSet2.iterator(); i.hasNext();) {
            node = returnAction(i.next(), false);
            ((SchVerb)node).index = rel;
            rel++;
            nodes.add(node);
        }
        return nodes;
    }


    /**
     * Saves story verbalization by Scheherazade built-in generator into a file
     * if file not specified outputs story verbalization to console
     * @throws Scheherexception
     * @throws IOException
     */
    void printScheherazadeRealization(String sch_file) throws Scheherexception, IOException {
        DiscoursePlan vglPlan = new DiscoursePlan(new VerbalizeTimeline(TimelineID.REALITY, VerbTense.PastSimple));
        String result;

        VerbalizerState vglVerbalizerState = verbalizer.freshState();
        vglVerbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

        try {
            result = verbalizer.verbalize(storyInterpreter, vglStyle, vglPlan, vglVerbalizerState).toString();
            if(sch_file != null && !sch_file.isEmpty()){
                // Write the scheherazade realization to the sch file
                FileWriter fw = new FileWriter(new File(sch_file));
                fw.write(result);
                fw.close();
            }
            else{System.out.println("Scheherazde built-in verbalization: " + result);}
        }

        catch (NullPlanResult npr) {
            System.out.println("No verbalization possible for "+vgl_file+".");
        }
    }

    /**
     * Creates the verbalization for the provided action (an annotation in Scheherazade)
     * @param action
     * @param perceptionTime
     * @return
     * @throws Scheherexception
     */
    public static String verbalizeAction(AssignedAction action, TransitionTime perceptionTime) throws Scheherexception  {
        Serialization verbalize = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(((AssignedAction) action), ((AssignedAction) action).getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState);
        return verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(((AssignedAction) action), ((AssignedAction) action).getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
    }


    /*******************************************************************************************************************/
    /*
    /*******************************************************************************************************************/

    /**
     * Given this node, and this list of nouns,
     * we iterate through all it's children to find coreferences
     * @param node: current node in the sentence
     * @param sentenceNouns: list of nouns already seen in the sentence
     * @throws Scheherexception
     */
    public static void findCoreferences(SchLexSynt node, ArrayList<SchNoun> sentenceNouns) throws Scheherexception {
        for (SchLexSynt child : node.getChildren()) { // iterate through children
            Boolean inlist = false;

            // see if noun has cocoreferenes
            if (child instanceof SchNoun) {
                testIfCoref((SchNoun) child, sentenceNouns, inlist);
            }
            findCoreferences(child, sentenceNouns);
        }

        // iterate through children again to recurse; only want to add nouns to the sentence when we actually encounter them;
        // NOT ahead of time
        //for (SchLexSynt child: node.getChildren()) { // iterate through children
        //    // check chidren of verb node for coreferences
        //    if (! (child instanceof SchNoun)) {
        //        findCoreferences(child, sentenceNouns);
        //    }
        //}
    }

    /**
     *
     * @param child: current node
     * @param sentenceNouns: list of nouns already seen in the sentence
     * @param inlist: indicating if we have seen this noun in the sentence already
     */
    public static void testIfCoref(SchNoun child, ArrayList<SchNoun> sentenceNouns, Boolean inlist) {
        for (SchNoun narr: sentenceNouns) {   // see if the character is already in the list
            // noun has a name
            if (!(child.name.equals("")) && narr.name.equals(child.name)) {
                inlist = true;
                break;}

            // noun doesn't have a name
            //if (displayDebug){
            //	System.out.println(narr.lexeme);
            //	System.out.println(child.lexeme);
            //}
            if (child.nounNameID.equals(narr.nounNameID)) {
                inlist = true;
                break;}
        }

        // ignore direct speech dummy nodes
        // don't need flavor (?)
        if (!child.isDirectSpeech() && !inlist ){ //&& child.flavor.toString().equals("character")) {
            sentenceNouns.add(child);} // add to the list if it's the first time seeing this noun
        else if (!child.isDirectSpeech() ){//&& child.flavor.toString().equals("character")) {
            child.setCoreference(true);} // change all other nouns to pronouns
    }

    /**
     * Recursively change nouns from 3rd person to 1st if the character is a narrator or if we are in a direct speech frame
     * speaker_type: noun, speaker_name: optional character name
     **/
    public static void setSpeaker(SchLexSynt node, String pov_type, String pov_name) throws Scheherexception{
        for (SchLexSynt child: node.getChildren()) {
            // check for noun children
            if (child instanceof SchNoun) {
                ((SchNoun)child).setSpeaker(pov_type, pov_name, ((SchNoun) child).possessive);

                // if there are any adjective childrn, add them as attribute to SchNoun node
                //if (pov_type.equals(((SchNoun)child).getLexeme())) {
                    ArrayList<SchLexSynt> removeAdjs = new ArrayList<SchLexSynt>();
                    //for (SchLexSynt grandchild : child.getChildren()) {
                    //    if (grandchild instanceof SchAdj) {
                    //        removeAdjs.add(grandchild);
                    //    }
                    //}
                    //for (SchLexSynt adj: removeAdjs) {
                        // add attribute
                    //    ((SchNoun)child).setProperty(((SchAdj)adj).getLexeme());

                        // remove node
                    //    child.getChildren().remove(adj);
                    //}
                //}

            }

            // handle nested functional words and children of nouns (possessive)
            setSpeaker(child, pov_type, pov_name);
        }
    }

    // assumes that this is a direct speech node
    public static void setSpeakerDS(SchLexSynt node, String pov_type, String pov_name, String speaker_type, String speaker_name) throws Scheherexception {
        for (SchLexSynt child: node.getChildren()) {
            // check for noun children
            if (child instanceof SchNoun) {
                System.out.println("POV name: "+ ((SchNoun) child).name + " " + pov_name);
                System.out.println("POV type: "+ ((SchNoun) child).lexeme + " " + pov_type);
                System.out.println("Speaker name: " + ((SchNoun) child).name + " " + speaker_name);
                System.out.println("Speaker type: " + ((SchNoun) child).lexeme + " " + speaker_type);
                // e.g. if 'the narrator' is the POV speaker, but not the utterance speaker --> NO change
                // if 'the narrator' is the POV speaker, and the utterance speaker --> CHANGE

                // in direct speech utterances, it doesn't matter who the POV is, only who the speaker is
                if ((pov_name != "" && ((SchNoun) child).name == pov_name) || ((SchNoun) child).getLexeme().equals(pov_type)) {
                    if ((speaker_name != "" && ((SchNoun) child).name == pov_name) || ((SchNoun) child).getLexeme().equals(speaker_type)) {
                        continue;
                    } else {
                        ((SchNoun) child).setSpeaker(speaker_type, speaker_name, ((SchNoun) child).possessive);
                    }
                } else {
                    ((SchNoun) child).setSpeaker(speaker_type, speaker_name, ((SchNoun) child).possessive);
                }
            }


            // handle nested functional words and children of nouns (possessive)
            setSpeakerDS(child, pov_type, pov_name, speaker_type, speaker_name);
        }
    }

    /**
     * Change the narrator in the dsynts array list before we generate to XML
     */
    private BuildDSyntS changeNarrator(BuildDSyntS dsynts, SchNoun narrator) throws Scheherexception {
        String narrator_type = narrator.lexeme; // narrator type
        String narrator_name = ""; // narrator name if applicable
        if (!((narrator).name.equals("")))
            narrator_name = narrator.name;

        // for each dsynt, recursively set the narrator to this character
        for (SchLexSynt root_verb: dsynts.getDsynts()) {
            if (((SchVerb)root_verb).isSpeech) {
                // in direct speech utterances, it doesn't matter who the POV is, only who the speaker is
                setSpeaker(root_verb, ((SchVerb) root_verb).speakerType, ((SchVerb) root_verb).speakerName);
            }
            else {
                setSpeaker(root_verb, narrator_type, narrator_name);
            }
        }
        return dsynts;
    }

    /*
     * Iterates through children of a verb and finds potential narrators
     * Keep them in global storyCharacters list
     * @param verb: root of the sentence
     */
    public static void addNarrators(SchLexSynt node) throws Scheherexception {
        if (node == null) {return;}
        for (SchLexSynt child: node.getChildren())  {
            boolean inlist = false; // is this noun already in the list?

            // if the child is a noun character
            if (child != null && child instanceof SchNoun && !((SchNoun)child).isDirectSpeech() && ((SchNoun)child).flavor != null
                    && !((SchNoun)child).nounNameID.contains("GROUP") && ((SchNoun)child).flavor.toString().equals("character")) {
                // empty list
                if (storyCharacters.isEmpty()) {
                    storyCharacters.add((SchNoun)child);
                    return;
                }

                else {
                    for (SchNoun narr: storyCharacters) {   // see if the character is already in the list
                        // noun has a name
                        if (!(((SchNoun)child).name.equals("")) && narr.name.equals(((SchNoun)child).name))  {inlist = true;}

                        // noun doesn't have a name
                        if ((((SchNoun)child).lexeme).equals(narr.lexeme)) {inlist = true;}
                    }
                    // add to the list
                    if (!inlist) {storyCharacters.add((SchNoun)child);}
                }
            }
            addNarrators(child);
        }
    }

    public static void getSentenceNouns(SchLexSynt node, ArrayList<SchNoun> sentenceNouns) throws Scheherexception {
        if (node == null) {return;}
        for (SchLexSynt child: node.getChildren())  {
            if (child != null && child instanceof SchNoun) {// && !((SchNoun)child).nounNameID.contains("GROUP")) {
            //if (child != null && child instanceof SchNoun && !((SchNoun)child).nounNameID.contains("GROUP")) {
                // make sure we don't add an embedded noun
                //if (child.getChildren().size() > 0) {
                //    for (SchLexSynt grandchild : child.getChildren()) {
                //        if (!(grandchild instanceof SchNoun)) {
                //            sentenceNouns.add((SchNoun) child);
                //        }
                //    }
                //}
                //else {
                    sentenceNouns.add((SchNoun) child);
                //}

            }
            if (child.getChildren().size() > 0) {getSentenceNouns(child, sentenceNouns);}
        }
    }


    /**
     *
     * handle if then statements
     formula: The narrator wanted it to snow (1) because (1.b) if it snowed (2) she would eat the snow. (3)
     The narrator thought (1) if it snowed (2) she would not allow for the groups to sled. (3)

     If there is a because, we start after it
     If not, we start with the children of the main verb
     Start: find all verbs that have an 'if' conditional,

     * DSYNTS
     (3) she would eat the snow
        (2) if it snowed
            (1.b) because
                (1) the narrator wanted it to snow
     */
    public static SchVerb findIfThenClause(SchVerb verb) throws Scheherexception{
        SchVerb newHead = null;
        SchVerb ifVerb = null;
        ArrayList<SchVerb> condVerbs = new ArrayList<SchVerb>();

        // iterate through children of this verb. If one of them is an if conditional, make the other verb a condition mood
        // TODO: need to account for any stray functional words
        for (SchLexSynt child : verb.getChildren()) {
            if (child instanceof SchVerb && ((SchVerb) child).conditionality != null && ((SchVerb) child).conditionality == Conditionality.If) {
                for (SchLexSynt otherChild : verb.getChildren()) {
                    if (otherChild instanceof SchVerb && ((SchVerb) otherChild).conditionality != null && ((SchVerb) otherChild).conditionality != Conditionality.If) {
                        // make the condition the head node
                        // get any other conditional verbs
                        if (newHead != null) {
                            ((SchVerb)otherChild).setMood("cond");
                            ((SchVerb)otherChild).index = 1;
                            condVerbs.add((SchVerb)otherChild);
                        }

                        else {
                            newHead = (SchVerb) otherChild;
                            newHead.setMood("cond");
                            newHead.index = 1;
                        }

                        // get if statement
                        if (ifVerb == null) {
                            ifVerb = (SchVerb) child;
                        }
                        verb.index = 0;
                    }
                }
            }
        }

        if (newHead != null) {
            verb.removeChild(newHead);
            // move the subject
            for (SchLexSynt subject : verb.getChildren()) {
                if (subject instanceof SchNoun && ((SchNoun) subject).index == 0) {
                    //otherChild.setChild(subject);
                    //verb.removeChild(subject);
                    break;
                }
            }

            // move any other verbs in the clause
            for (SchVerb otherVerb : condVerbs) {
                verb.removeChild(otherVerb);
                newHead.setChild(otherVerb);
            }

            // append an if clause to the new head node
            SchAttr ifNode = new SchAttr("ATTR", "IF");
            ifNode.setRelation("ATTR");
            newHead.setChild(ifNode);
            ifNode.setChild(ifVerb);
            ((SchVerb) ifVerb).index = 1;
            verb.removeChild(ifVerb);

            // append old head verb to new head node and set to index = 1
            newHead.setChild(verb);
            verb.index = 0;
            return newHead;
        }

        return verb;
    }



    // TODO asap: remove the following two methods for getting interpretation sequence; we're trying a different way to access these things
    public static void getTrace(String originalStory) throws Scheherexception{
        AssignedActionSet actionSetLocal = storyInterpreter.getAllActions(TimelineID.REALITY);
        for (Iterator<AssignedAction> i = actionSetLocal.iterator(); i.hasNext(); ) {
            verbalizerState = verbalizer.freshState();
            verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

            AssignedAction action = i.next();
            String sig_def = action.render().toString();
            if (sig_def.contains("interp")){continue;}
            TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));
            String actionVerbalization = "";

            String storyPointText = originalStory.substring(action.getSourceSpan().begin(), action.getSourceSpan().end());
            System.out.println(storyPointText);

        }
    }

    /**
     * Call on the root action or any subsequent modifier actions
     * IN PROGRESS
     *
     */
    public static ArrayList<Object> getInterpSequence(Object head, ArrayList<Object> actions) throws Scheherexception {
        Object child = null;
        Object grandchild = null;
        //if (head instanceof AssignedAction || head instanceof AssignedCondition || head instanceof ValidAction) { // || head instanceof AssignedPredicate) {
        if (head instanceof AssignedPredicate && !((AssignedPredicate) head).getNoInterpretation()) {
            actions.add(head);
        }

        if (head instanceof AssignedAction) {
            for (Enumeration<Object> k = ((AssignedAction) head).getAction().getSCHArguments().elements(); k.hasMoreElements(); ) {
                child = k.nextElement();
                if (child instanceof AssignedAction || child instanceof AssignedCondition || child instanceof AssignedModifier
                        || child instanceof AssignedPredicate || child instanceof ValidAction || child instanceof ValidCondition) {
                    getInterpSequence(child, actions);
                }
            }
        }

        else if (head instanceof ValidAction || head instanceof ValidCondition) {
            for (Enumeration<Object> k = ((ValidPredicate) head).getSCHArguments().elements(); k.hasMoreElements(); ) {
                child = k.nextElement();
                if (child instanceof AssignedAction || child instanceof AssignedCondition || child instanceof AssignedModifier
                        || child instanceof AssignedPredicate || child instanceof ValidAction || child instanceof ValidCondition) {
                    getInterpSequence(child, actions);
                }
            }
        }

        else if (head instanceof AssignedCondition) {
            for (Enumeration<Object> k = ((AssignedCondition) head).getCondition().getSCHArguments().elements(); k.hasMoreElements(); ) {
                child = k.nextElement();
                if (child instanceof AssignedAction || child instanceof AssignedCondition || child instanceof AssignedModifier
                        || child instanceof AssignedPredicate || child instanceof ValidAction || child instanceof ValidCondition) {
                    getInterpSequence(child, actions);
                }
            }
        }

        else if (head instanceof AssignedModifier) {
            // recurse on children
            for (Enumeration<Object> i = ((AssignedModifier) head).getModifier().getSCHArguments().elements(); i.hasMoreElements(); ) {
                child = i.nextElement();
                if (child instanceof AssignedAction || child instanceof AssignedCondition || child instanceof AssignedModifier
                        || child instanceof AssignedPredicate || child instanceof ValidAction || child instanceof ValidCondition) {
                    getInterpSequence(child, actions);
                }
            }
        }
        // only interested in clauses and their children (grandchildren), not adverbs
       else if (head instanceof AssignedPredicate) {
            for (Iterator<AssignedModifier> j = storyInterpreter.getModifiersOfPredicate((AssignedPredicate) head).iterator(); j.hasNext(); ) {
                child = j.next();
                if (child instanceof AssignedModifier) {
                    // recurse on grandchildren
                    for (Enumeration<Object> i = ((AssignedModifier) child).getModifier().getSCHArguments().elements(); i.hasMoreElements(); ) {
                        grandchild = i.nextElement();
                        if (grandchild instanceof AssignedAction || grandchild instanceof AssignedCondition || grandchild instanceof AssignedModifier
                                || grandchild instanceof AssignedPredicate || grandchild instanceof ValidAction || grandchild instanceof ValidCondition) {
                            getInterpSequence(grandchild, actions);
                        }
                    }
                }
            }
        }

        else if (head instanceof ValidTimeline) {

        }

       if (head instanceof AssignedPredicate) {
           for (Iterator<AssignedModifier> j = storyInterpreter.getModifiersOfPredicate((AssignedPredicate) head).iterator(); j.hasNext(); ) {
               child = j.next();
               getInterpSequence(child, actions);
           }
       }

       return actions;
    }

    /**
     * IN PROGRESS
     * @param actions
     * @throws Scheherexception
     */
    public static void getInterpSequence(ArrayList<Object> actions) throws Scheherexception {
        for (Object action : actions) {
            // for each type of arc type that exists (provides for, attempt to cause, etc...) is there one attached to this action?
            for (Iterator<InterpretativeArcType> a = InterpretativeArcType.getAllTypes().iterator(); a.hasNext(); ) {
                InterpretativeArcType arcType = a.next();
                // Is there an interp element linked by this arc type?
                InterpArcSet arcs = storyInterpreter.getLinkedInterpElements(TimelineID.REALITY, (InterpElement)action, arcType, LinkDirection.FromNode);

                // found at least one link of type "arcType" from this "action"
                for (Iterator<InterpArc> j = arcs.iterator(); j.hasNext(); ) {
                    InterpArc arc = j.next();
                    String toVerbalization = "";
                    String fromVerbalization = "";

                    verbalizerState = verbalizer.freshState();
                    verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

                    if (arc.to() instanceof AssignedAction) {
                        AssignedAction to = (AssignedAction) arc.to();
                        try {
                            displayAction(to);//toVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(((AssignedAction) to), ((AssignedAction) to).getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
                        } catch (Scheherexception sch) {
                            System.out.println("No verbalization possible for " + to.getAction().getType().toString() + ".");
                        }
                    } else if (arc.to() instanceof AssignedInterpNode) { //life, love, health, ...
                        AssignedInterpNode to = (AssignedInterpNode) arc.to();
                        toVerbalization = "This would " + arc.arcType() +" "+ to.getCharacter().getType().toString() + ":" + to.getInterpretativeNodeType().toString();
                    } else if (arc.to() instanceof AssignedCondition) { // property
                        AssignedCondition to = (AssignedCondition) arc.to();
                        toVerbalization = to.getCondition().getType().toString();
                    }
                    System.out.println(arc.arcType().toString() +" "+ toVerbalization);
                }
            }
        }
    }

    /**
     * Iterate through the story and get paits of nodes connected by an arc in the SIG
     *  timeline -> timeline
     *  timeline -> interp
     *  interp -> interp
     *
     * @throws Scheherexception
     */
    public void getEventPairs() throws Scheherexception {
        AssignedActionSet actionSetLocal = storyInterpreter.getAllActions(TimelineID.REALITY);

        String pair1 = "";
        for (Iterator<AssignedAction> i = actionSetLocal.iterator(); i.hasNext();) {
            verbalizerState = verbalizer.freshState();
            verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

            AssignedAction action = i.next();
            String sig_def = action.render().toString();
            TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));
            String actionVerbalization = "";

            // get timeline pairs
            if (!sig_def.contains("interp")){
                try{
                    actionVerbalization = verbalizeAction(action, perceptionTime);

                    if (pair1 == "") {pair1 =  actionVerbalization;}  // initialize the pair
                    else { // add each pair of events in the timeline
                        ArrayList pair = new ArrayList();
                        pair.add("FROM:"+pair1);
                        pair.add("TO:"+actionVerbalization);
                        pair.add("ARC:timeline");
                        storyGraph.add(pair);
                        pair1 = actionVerbalization;
                    }
                }
                catch (Scheherexception sch) {System.out.println("No verbalization possible for "+((AssignedAction) action).getAction().getType().toString()+".");}
            }
        }

        // We want the root action of the sentence assosiated with the interpretive element, not any modifiers
        // We need to reassign the modifiers with that link
        // To do this, we recreate all the links and add them to this new set
        // InterpArc newArc = new InterpArc(root, arc.arcType(), root);
        InterpArcSet newSet = new InterpArcSet();
        AssignedAction linkedNode = null;
        BuildDSyntS dsyntsTest = new BuildDSyntS(); // here for testing

        // attempt to get the interpretative states and arcs that are linked to a given assigned action
        // For each interpretative arc type that can come from here...
        for (Iterator<AssignedAction> i = actionSetLocal.iterator(); i.hasNext();) {
            verbalizerState = verbalizer.freshState();
            verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);


            AssignedAction root = i.next();
            linkedNode = root;
            TransitionTime perceptionTime = new TransitionTime(new StateTime(StateTimeType.Present, 0), new StateTime(StateTimeType.Present, 1000));
            // TODO: look a the root action as well as any modifiers

            // we only want to look at actions from the timeline layer that have interpretative links
            if (root.isInterp() && root.getNoInterpretation()) {
                // TODO: if the root has no interpretive links, check the children for links
                // for modifiers
                // if child
            }

            // for each type of arc type that exists (provides for, attempt to cause, etc...) is there one attached to this action?
            for (Iterator<InterpretativeArcType> a = InterpretativeArcType.getAllTypes().iterator(); a.hasNext(); ) {
                InterpretativeArcType arcType = a.next();
                // Is there an interp element linked by this arc type?
                InterpArcSet arcs = storyInterpreter.getLinkedInterpElements(TimelineID.REALITY, linkedNode, arcType, LinkDirection.FromNode);

                // found at least one link of type "arcType" from this "root"
                for (Iterator<InterpArc> j = arcs.iterator(); j.hasNext(); ) {
                    InterpArc arc = j.next();
                    String toVerbalization = "";
                    String fromVerbalization = "";



                    verbalizerState = verbalizer.freshState();
                    verbalizerState.setVerbTense(VerbalizerState.PREVAILING_TENSE, VerbTense.PastSimple);

                    if (arc.to() instanceof AssignedAction) {
                        AssignedAction to = (AssignedAction) arc.to();
                        try{
                            toVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(((AssignedAction) to), ((AssignedAction) to).getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
                        } catch (Scheherexception sch) {System.out.println("No verbalization possible for "+((AssignedAction) to).getAction().getType().toString()+".");}
                    }
                    else if (arc.to() instanceof AssignedInterpNode) { //life, love, health, ...
                        AssignedInterpNode to = (AssignedInterpNode) arc.to();
                        toVerbalization = to.getCharacter().getType().toString() +":"+ to.getInterpretativeNodeType().toString();
                    }
                    else if (arc.to() instanceof AssignedCondition) { // property
                        AssignedCondition to = (AssignedCondition) arc.to();
                        toVerbalization = to.getCondition().getType().toString();
                    }


                    if (arc.from() instanceof AssignedAction) {
                        // need to check for nested action nodes
                        AssignedAction from = (AssignedAction) arc.from();
                        try{
                            fromVerbalization = verbalizer.verbalize(storyInterpreter, Style.DEFAULT_STYLE, new VerbalizeAssignedAction(((AssignedAction) from), ((AssignedAction) from).getTime(), perceptionTime, VerbTense.PastSimple, false), verbalizerState).toString();
                        } catch (Scheherexception sch) {System.out.println("No verbalization possible for "+((AssignedAction) from).getAction().getType().toString()+".");}
                    }
                    else if (arc.from() instanceof AssignedInterpNode) { //life, love, health, ...
                        AssignedInterpNode from = (AssignedInterpNode) arc.from();
                        fromVerbalization = from.getCharacter().getType().toString() +":"+ from.getInterpretativeNodeType().toString();
                    }
                    else if (arc.from() instanceof AssignedCondition) { // property
                        AssignedCondition from = (AssignedCondition) arc.from();
                        fromVerbalization = from.getCondition().getType().toString();
                    }

                    ArrayList pair = new ArrayList();
                    pair.add("FROM:"+fromVerbalization);
                    pair.add("TO:"+toVerbalization);
                    pair.add("ARC:"+arc.arcType().toString());

                    // e.g. The lion's health was provided for.
                    // The man's life was damaged.

                    if (arc.to() instanceof AssignedInterpNode) { //life, love, health, ...
                        String arcString = arc.arcType().toString();
                        AssignedInterpNode to = (AssignedInterpNode) arc.to();
                        String nounName = to.getCharacter().getType().toString();
                        SchVerb verb = new SchVerb("be");
                        SchNoun obj = new SchNoun(to.getInterpretativeNodeType().toString(), 0);
                        obj.setChild(new SchNoun(to.getCharacter().getType().toString().substring(nounName.indexOf(':')+1, nounName.indexOf('>')), 0));
                        verb.setChild(obj);

                        // split provide for
                        if (arcString.equals("providesFor")){
                            SchVerb verb2 = new SchVerb("provide");
                            verb2.setChild(new SchFunc("for"));
                            verb.setChild(verb2);
                        }
                        // remove tense from others
                        else if (arcString.equals("damages") || arcString.equals("actualizes")){
                            SchVerb verb2 = new SchVerb(arcString.substring(0, arcString.length()-1));
                            verb.setChild(verb2);
                        }
                        else {continue;}
                        dsyntsTest.addDSynt(verb);
                    }
                    String directory = "C:\\Users\\Stephanie\\scheherazade-0.34-src\\scheherazade\\es-translator\\data\\";
                    String xml_file = directory + "xml/dev/test.xml";
                    dsyntsTest.saveDSynt(xml_file);

                    storyGraph.add(pair);
                    System.out.println(fromVerbalization +"\n"+ toVerbalization +"\n"+ arc.arcType().toString() +"\n\n");
                }
            }
        }
    }




}