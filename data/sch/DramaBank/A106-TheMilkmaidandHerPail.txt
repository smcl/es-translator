There once was a milkmaid and a group of cows.  The milkmaid was milking the group of cows.  

The milkmaid walked toward a dairy and carried a pail on the head of the milkmaid.  

The pail contained some milk.  

The milkmaid began to plan for the milk to later transform into some cream, for the milkmaid to later make the cream into some butter and to later sell the butter at a market, for she to later buy some eggs, for a group of chickens to later hatch from the eggs, for the milkmaid to later sell it, to later buy a gown and for she to later wear it at a fairground, for every fellow to later admire the gown and to later court the milkmaid, and for the milkmaid to later shake the head of the milkmaid and to later ignore every fellow.  

The milkmaid shook the head of the milkmaid.  

The pail fell, and the milk spilled.  

It began to not be possible for the milk to later transform into the cream, for the milkmaid to later make the cream into the butter and to later sell the butter at the market, for she to later buy the eggs, for the group of chickens to later hatch from the eggs, for the milkmaid to later sell it, to later buy the gown and for she to later wear it at the fairground, for every fellow to later admire the gown and to later court the milkmaid, and for the milkmaid to later shake the head of the milkmaid and to later ignore every fellow.  

