
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.elements.AssignedModifier;
import story.scheherazade.misc.ActionType;
import story.scheherazade.misc.ConditionType;
import story.scheherazade.misc.NounName;
import story.scheherazade.verbalizer.Verbalizer;
import story.scheherazade.verbalizer.VerbalizerState;
import story.scheherazade.verbalizer.ModifierGrammaticalType;
import story.scheherazade.elements.ValidAction;

/**
 * Lexico-syntactic class for Adjectives
 */
public class SchAdj implements SchLexSynt{
    /** */
	String lexeme;
	String offset;

    /** */
	boolean preModifier;

    /** */
	ArrayList<SchLexSynt> children;
	SchLexSynt parent;

    /**
     *
     * @param info
     * @throws Scheherexception
     */
	public SchAdj(String info) throws Scheherexception {
		this.lexeme = getLexeme(info);
		info = info.replaceAll("[\\<\\>\\[\\]]", "");
		offset = "-1";
		if (info.contains("#adj")) {
			String[] schLexeme = info.split("#");
			String temp = schLexeme[1].replace("adj0", "");
			// if there is a decimal (for some reason split(".") doesn't work)
			if (!Character.isDigit(temp.charAt(temp.length()-2))) {
				offset = temp.substring(0, temp.length()-2);
			}
			else {offset = temp;}
		}
		children = new ArrayList<SchLexSynt>();
	}

	public String getLexeme() {return lexeme;}

    public SchAdj createCopy() {
        SchAdj newAdj = null;
        try {
            newAdj = new SchAdj("");
            newAdj.lexeme = this.lexeme;
			newAdj.offset = this.offset;
            newAdj.preModifier = this.preModifier;
            for (SchLexSynt child : this.children) {
                //newAdj.children.add(child.createCopy());
				newAdj.setChild(child.createCopy());
            }
        } catch (Scheherexception e) {System.out.println(e);}
        return newAdj;
    }

    /**
     *
     */
	public void setPremodifier(){
		preModifier = true;
	}

    /**
     *
     * @param info
     * @return
     */
	public String getLexeme(String info) {
		info = info.replaceAll("[\\<\\>\\[\\]]", "");
		String[] schLexeme = info.split("#");
		return schLexeme[0];
	}

    /**
     * when 2 adverbs are coordinated by 'and', second should have 'II' relation
     * @return if the
     */
	public String getRelation(){
		return (preModifier ? "ATTR" : "II");
	}

	public String getOffset() {return offset;}
    /******************************************************************************************************************/

    /**
     *
     * @return
     */
	public Map<String,String> getFeatures(){
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("class", "adjective");
		features.put("lexeme", lexeme);
		features.put("rel", getRelation());
		features.put("wn_offset", getOffset());
		return features;
	}

    /**
     * Returns the elements this node governs
     * @return
     */
	public ArrayList<SchLexSynt> getChildren(){
		return children;
	}

    /**
     * Sets the elements this node governs
     * @param child
     */
	public void setChild(SchLexSynt child){
		this.children.add(child);
		child.setParent(this);
	}

	public void setParent(SchLexSynt p) {parent = p;}
	public SchLexSynt getParent() {return parent;}

    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

}
